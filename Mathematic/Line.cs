﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace ImgModLib.Mathematic
{
    public class Line
    {
        PointF p1, p2;

        public PointF P1 
        { 
            get { return p1; }
            set { p1 = value; }
        }

        public PointF P2 
        { 
            get { return p2; }
            set { p2 = value; }
        }

        public Line(PointF p1, PointF p2)
        {
            if (p1.Y == p2.Y && p1.X == p2.X)
                throw new Exception("Невозможно задать уравнение прямой на основе одной точки");

            this.p1 = p1;
            this.p2 = p2;
        }

        public int BecomeClearHalfPlane(PointF point)
        {
            float dx = P2.X - P1.X, dy = P2.Y - P1.Y;
            float x = point.X, y = point.Y;

            if (dy * x - dx * y < dy * P1.X - dx * P1.Y)
                return -1;

            if (dy * x - dx * y > dy * P1.X - dx * P1.Y)
                return 1;
            
            return 0;
        }

        public static bool IsOneLine(Line line1, Line line2)
        {
            return line1.BecomeClearHalfPlane(line2.P1) == 0
                && line1.BecomeClearHalfPlane(line2.P2) == 0;
        }

        public bool Contains(PointF point)
        {
            return BecomeClearHalfPlane(point) == 0
                   && ((p1.X >= point.X && p2.X <= point.X) || (p2.X >= point.X && p1.X <= point.X))
                   && ((p1.Y >= point.Y && p2.Y <= point.Y) || (p2.Y >= point.Y && p1.Y <= point.Y));
        }

        public static bool HasIntersection(Line line1, Line line2)
        {
            if (Line.IsOneLine(line1, line2) 
                && (line1.Contains(line2.P1) || line1.Contains(line2.P2)))
                return true;

            if (line1.BecomeClearHalfPlane(line2.P1) == line1.BecomeClearHalfPlane(line2.P2))
                //|| (Math.Abs(line1.BecomeClearHalfPlane(line2.P1) + line1.BecomeClearHalfPlane(line2.P2)) == 1))
                return false;

            if (line2.BecomeClearHalfPlane(line1.P1) == line2.BecomeClearHalfPlane(line1.P2))
                //|| (Math.Abs(line2.BecomeClearHalfPlane(line1.P1) + line2.BecomeClearHalfPlane(line1.P2)) == 1))
                return false;

            return true;
        }

        public bool HasIntersection(Line line)
        {
            return Line.HasIntersection(this, line);
        }

        public override string ToString()
        {
            return "(" + p1.X + "," + p1.Y + ") (" + p2.X + "," + p2.Y + ")";
        }

        internal PointF GetMiddle()
        {
            return new PointF((p1.X + p2.X) / 2, (p1.Y + p2.Y) / 2);
        }
    }
}
