﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;

namespace ImgModLib.Mathematic
{
    public class Polygon
    {
        protected PointF[] points = null;
        Line[] lines = null;

        public PointF this[int index]
        {
            get { return points[index]; }
            set { points[index] = value; }
        }
        
        public int NumPoints { get { return points.Length; } }

        public float Left
        {
            get { return points.Min(point => point.X); }
        }

        public float Right
        {
            get { return points.Max(point => point.X); }
        }

        public float Top
        {
            get { return points.Max(point => point.Y); }
        }

        public float Bottom
        {
            get { return points.Min(point => point.Y); }
        }

        public Polygon(PointF[] points)
        {
            List<PointF> pointsList;
            List<Line> linesList, newLineList;

            if (points == null)
                throw new Exception("Не задан массив точек, на отснове которых строится полигон");

            if (points.Length < 3)
                throw new Exception("Недостаточно точек для построеня полигона");

            for (int i = 0; i < points.Length; i++)
            {
                if (points[i] == null)
                    throw new Exception("В массиве не заданы значания некоторых точек");
            }

            if (points.Count(point => point.X == points[0].X) == points.Length
                || points.Count(point => point.Y == points[0].Y) == points.Length)
                throw new Exception("Координаты прямоугольника заданы некорректно, так как полигон не может состоять из одной линии.");

            foreach (PointF point in points)
            {
                PointF[] selpoints = points.Where(p => p.X == point.X && p.Y == point.Y).ToArray();

                if (selpoints.Length > 1)
                    throw new Exception("Массив содержит совпадающие точки");
            }

            int lenght = points.Length;
            pointsList = new List<PointF>();
            linesList = new List<Line>();
            newLineList = new List<Line>();
            
            for (int i = 0; ; i++)
            {
                Line addline = new Line(points[i], points[(i + 1) % lenght]);
                Line line; 
                
                int j = (i + 1) % lenght;

                line = new Line(points[j], points[(j + 1) % lenght]);
                
                while(Line.IsOneLine(addline, line))
                {
                    addline.P2  = points[(j + 1) % lenght];
                    
                    j = (j + 1) % lenght;
                    
                    line.P1 = points[j];
                    line.P2 = points[(j + 1) % lenght];
                }

                linesList.Add(addline);
                i = j - 1;
                if (i == -1) break;
            }

            if (Line.IsOneLine(linesList.First(), linesList.Last()))
            {
                Line line = new Line(linesList.Last().P1, linesList.First().P2);

                linesList.Remove(linesList.Last());
                linesList.Remove(linesList.First());
                linesList.Insert(0, line);
            }

            lines = linesList.ToArray();

            foreach (Line line in lines)
            {
                pointsList.Add(line.P1);
            }

            for (int i = 0; i < lines.Length; i++)
            {
                for (int j = 0; j < lines.Length - 3; j++)
                {
                    if (lines[i % lines.Length].HasIntersection(lines[(i + j + 2) % lines.Length]))
                        throw new Exception("Координаты прямоугольника заданы некорректно, так как его ребра пересекаются.");
                }
            }

            if (IsClockwise(pointsList))
                pointsList.Reverse();

            this.points = pointsList.ToArray();
        }

        private bool IsClockwise(List<PointF> points)
        {
            PointF prevminp, nextminp;
            PointF minpoint = points[0];
            int imin = 0;

            for (int i = 1; i < points.Count; i++)
            {
                if (points[i].X < minpoint.X)
                {
                    minpoint = points[i];
                    imin = i;
                }
            }

            prevminp = (imin == 0) ? points[points.Count - 1] : points[imin - 1];
            nextminp = (imin == points.Count - 1) ? points[0] : points[imin + 1];

            if (Determinant
                (
                    new float[,]
                    {
                        {prevminp.X - minpoint.X, prevminp.Y - minpoint.Y},
                        {nextminp.X - minpoint.X, nextminp.Y - minpoint.Y}                        
                    }
                ) > 0)
                return false;
            else
                return true;
        }

        public bool HasIntersection(Line eq)
        {
            foreach (Line line in lines)
            {
                if (line.HasIntersection(eq))
                    return true;
            }

            return false;
        }

        public void ParallelTranslate(float dx, float dy)
        {
            for (int i = 0; i < points.Length; i++)
            {
                points[i].X += dx;
                points[i].Y += dy;
            }
        }

        public void Rotate(PointF center, float angle)
        {
            float sin = (float)Math.Sin(angle);
            float cos = (float)Math.Cos(angle);
            float dx, dy;

            for (int i = 0; i < points.Length; i++)
            {
                dx = points[i].X - center.X;
                dy = points[i].Y - center.Y;

                points[i].X = center.X + dx * cos - dy * sin;
                points[i].Y = center.Y + dx * sin + dy * cos;
            }
        }

        float Determinant(float[,] arr)
        {
            return arr[0, 0] * arr[1, 1] - arr[1, 0] * arr[0, 1];
        }

        public void Draw(Color color, float width, DashStyle dashStyle, Graphics g, bool needSegmentation)
        {
            Pen pen = new Pen(color, width);
            Font font = new Font(FontFamily.GenericSerif.Name, 10);
            int numpoints = points.Length;
            List<PointF> listpoints;

            pen.DashStyle = dashStyle;
            for (int i = 0; i < numpoints - 1; i++)
            {
                PointF p1 = points[i];
                PointF p2 = points[i + 1];

                g.DrawLine(pen, p1, p2);                
            }
                        
            g.DrawLine(pen, points[numpoints - 1], points[0]);

            if (!needSegmentation) return;

            pen.DashStyle = DashStyle.Dash;
            //pen.DashPattern = new float[] { 3, 3, 3 };
            pen.Color = Color.Black;
            listpoints = points.ToList();

            while (numpoints > 3)
            {
                float mindiagonal = float.MaxValue;
                int imin = 0, h, j;

                FindMinDiagonal(listpoints, ref mindiagonal, ref imin);

                h = (imin == 0) ? numpoints - 1 : imin - 1;
                j = (imin == numpoints - 1) ? 0 : imin + 1;

                g.DrawLine(pen, listpoints[h], listpoints[j]);
                numpoints--;

                listpoints.RemoveAt(imin);
            }
        }

        void FindMinDiagonal(List<PointF> points, ref float mindiagonal, ref int imin)
        {
            int numpoints = points.Count;

            for (int i = 0; i < points.Count; i++)
            {
                PointF p1, p2, p3;
                float dx12, dy12, dx13, dy13;
                float determinant, diagonal;
                Line line;
                int j = 0, h = 0;

                h = (i == 0) ? numpoints - 1 : i - 1;
                j = (i == numpoints - 1) ? 0 : i + 1;
                
                p1 = points[h];
                p2 = points[i];
                p3 = points[j];
                line = new Line(p1, p3);

                dx12 = p2.X - p1.X; dy12 = p2.Y - p1.Y;
                dx13 = p3.X - p1.X; dy13 = p3.Y - p1.Y;

                determinant = Determinant(
                        new float[,]
                        {
                            {dx12, dy12},
                            {dx13, dy13}
                        });
                
                diagonal = (float)Math.Sqrt(dx13 * dx13 + dy13 * dy13);

                if (determinant < 0 && diagonal < mindiagonal && !HasIntersection(line)
                    && Contains(line.GetMiddle()))
                {
                    mindiagonal = diagonal;
                    imin = i;
                }
            }
        }

        public bool Contains(PointF point)
        {
            Line testline = new Line(point, new PointF(-1, -1));
            int numintersects = 0;

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].HasIntersection(testline))
                    numintersects++;

                if (Math.Abs(testline.BecomeClearHalfPlane(lines[i].P1)
                    + testline.BecomeClearHalfPlane(lines[i].P2)) == 1)
                    i++;
            }

                //foreach (Line line in lines)
                //{
                //    if (line.HasIntersection(testline))
                //        numintersects++;
                //}

            return (numintersects % 2) != 0;
        }

        int GetCode(PointF point, RectangleF rect)
        {
            int rez = 0;

            rez |= Convert.ToInt32(point.X < rect.X);
            rez <<= 1;
            rez |= Convert.ToInt32(point.X > rect.X + rect.Width);
            rez <<= 1;
            rez |= Convert.ToInt32(point.Y < rect.Y - rect.Height);
            rez <<= 1;
            rez |= Convert.ToInt32(point.Y > rect.Y);

            return rez;
        }

//        public void Clip(RectangleF clipRect)
//        {
//            //if (clipRect.Width == 0 || clipRect.Height == 0 
//            //    || !clipRect.IntersectsWith(new RectangleF(Left, Top, Right - Left, Top - Bottom)))
//            //    throw new InvalidOperationException("Область отсечения задана некорректно");
//
//            //for (int i = 0; i < points.Length; i++)
//            //{
//            //    PointF p, before_p, after_p;
//
//            //    p = points[i];
//            //    before_p = points[]
//            //}
//        }
    }
}
