﻿/*
 * Created by SharpDevelop.
 * UserFilter: админ
 * Date: 23.09.2011
 * Time: 9:37
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

namespace ImgModLib.Mathematic
{
	/// <summary>
	/// Description of Matrix.
	/// </summary>
	public class Matrix : ICloneable
	{
		double[,] array;
		
		public double this[int index1, int index2]
		{
			get { return array[index1, index2]; }
			set { array[index1, index2] = value; }
		}
		
		public int Width { get { return array.GetLength(1); } }
		public int Height { get { return array.GetLength(0); } }

		public double Determinant
		{
			get
			{	
				if (Width != Height) return double.NaN;
				
				double determinant = 1;
				int n = Width, i, j, s = 0;
				
				double[,] buf = (double[,])array.Clone();
				for (int r = 0; r < n-1; r++)
				{
      				//{Если ел-т главной диагонали равен 0, то ищем ненулевой ел-т в данном столбце}
      				if (array[r,r] == 0)
      				{
      					for (i = r+1; i < n; i++) if (array[i,r] != 0) break;
    	  				
      					if (i < n)
      					{
      						for (j = 0; j < n; j++)
      						{
	      						double pr;
      						
	      	  					pr = array[i,j];			//{Меняем местами целиком строки с}
	        					array[i,j] = array[r,j];	//{нулевым и с ненулвевым елементами}
		        				array[r,j] = pr; 							
	      					}
	      					s++;
      					}
      					else
      					{
      						array = buf;
      						return 0;
      					}
      				}				

      				for (i = r+1; i < n; i++)
      				{
      					double pr = 0;
 	      				if (array[i,r] != 0)
 	      				{
          					//{Приводим матрицу к треугольному виду}
     	    				pr = array[i,r] / array[r,r];
     	    				for (j = r; j < n; j++) array[i,j] -= pr * array[r,j];
 	      				}
      				}          //{Считаем определитель,}
   	  				determinant *= array[r,r]; //{Перемножая элементы на главной диагонали}
				}
 	  			determinant *= array[n-1, n-1];	

				array = buf;
				return Math.Pow(-1, s) * determinant;
			}
		}
		
		public Matrix(int width, int height)
		{
			if (width <= 0)
				throw new Exception("Ширина должна быть положительной");
			
			if (height <= 0)
				throw new Exception("Высота должна быть положительной");
			
			array = new double[height, width];
			
			for (int i = 0; i < height; i++)
				for (int j = 0; j < width; j++)
					array[i, j] = 0;
		}
		
		public Matrix(double[,] array)
		{
			if (array == null)
				throw new Exception("Массив не задан!");
			
			this.array = array;
		}
		
		public static Matrix IdentityMatrix(int n)
		{
			Matrix rezmatrix;
			
			if (n < 1) throw new Exception("Невозможно создать матрицу");
			
			rezmatrix = new Matrix(n, n);
			
			for (int i = 0; i < n; i++)
				rezmatrix[i,i] = 1;
			
			return rezmatrix;
		}
		
		public Matrix GetTransposedMatrix()
		{
			Matrix TransposedMatrix = new Matrix(Height, Width);
			
			for (int i = 0; i < TransposedMatrix.Height; i++)
				for (int j = 0; j < TransposedMatrix.Width; j++)
					TransposedMatrix[i, j] = this[j, i];
			
			return TransposedMatrix;
		}
			
		public Matrix GetSubMatrix(int startRow, int startCollum, int numRows, int numCollums)			
		{
			if (startRow < 0 || startRow + numRows > Height
			    || startCollum < 0 || startCollum + numCollums > Width)
				throw new IndexOutOfRangeException("Невозможно выделить подматрицу с заданными параметрами");

			Matrix rez = new Matrix(numCollums, numRows);
			
			for (int i = 0, m = startRow; i < numRows; i++, m++)
			{
				for (int j = 0, k = startCollum; j < numCollums; j++, k++)
				{
					rez[i, j] = array[m, k];					
				}
			}
			
			return rez;
		}
		
		public Matrix GetRow(int numrow)
		{
			return GetSubMatrix(numrow, 0, 1, Width);
		}
		
		public Matrix GetCollum(int numcollum)
		{
			return GetSubMatrix(0, numcollum, Height, 1);
		}
		
		public Matrix GetReverseMatrix()
		{
			Matrix ReverseMatrix, CopyMatrix;
			int n, r, i, j;
			double pr;
			
			if (Width != Height || Determinant == 0) return null;		
			
			n = Width;
			CopyMatrix = (Matrix)this.Clone();
			ReverseMatrix = Matrix.IdentityMatrix(Width);
			
			for (r = 0; r < n; r++)
			{
				//{Если ел-т главной диагонали равен 0, то ищем ненулевой ел-т в данном столбце}
				if (array[r,r] == 0)
				{
					for (i = r+1; i < n; i++) if (CopyMatrix[i,r] != 0) break;
					for (j = 0; j < n; j++)
					{
        				pr = CopyMatrix[i,j];
        				CopyMatrix[i,j] = CopyMatrix[r,j];
             			CopyMatrix[r,j] = pr;      			//{Меняем местами целиком строки с}
        				
             			pr = ReverseMatrix[i,j];      		//{нулевым и с ненулвевым елементами}   						
        				ReverseMatrix[i,j] = ReverseMatrix[r,j];
        				ReverseMatrix[r,j] = pr;
					}
				}			

    			pr = CopyMatrix[r, r];
    			for (j = 0; j < n; j++) 	//{Нормироание строки матрицы}
				{
			  		CopyMatrix[r, j] /= pr;
			  		ReverseMatrix[r, j] /= pr;
				}

				for (i = 0; i < n; i++)
    			{	
    				if (r != i)
    				{
    					pr = CopyMatrix[i, r];
    					for (j = 0; j < n; j++)
    					{
				  			CopyMatrix[i, j] -= pr * CopyMatrix[r, j];
	    	    			ReverseMatrix[i,j] -= pr * ReverseMatrix[r, j];
    					}
					}
    			}
			}

			return ReverseMatrix;    		
		}
		
		public object Clone()
		{
			Matrix rezmatrix = new Matrix(Width, Height);
			
			for (int i = 0; i < Width; i++)
				for (int j = 0; j < Height; j++)
					rezmatrix[i,j] = this[i,j];
			
			return rezmatrix;
		}
		
		public static Matrix operator * (Matrix matrix, double val)
		{
			Matrix rezmatrix = new Matrix(matrix.Width, matrix.Height);
			
			for (int i = 0; i < matrix.Height; i++)
				for (int j = 0; j < matrix.Width; j++)					
					rezmatrix[i, j] = matrix[i, j] * val;
			 
			return rezmatrix;
		}

		public static Matrix operator * (double val, Matrix matrix)
		{
			return matrix * val;
		}
		
		public static Matrix operator * (Matrix m1, Matrix m2)
		{
			Matrix rezmatrix;
			
			if (m1.Width != m2.Height)
				throw new Exception("Матрицы невозможно перемножить");
			
			rezmatrix = new Matrix(m2.Width, m1.Height);
						
			for(int i = 0; i < m1.Height; i++)
				for(int j = 0; j < m2.Width; j++)
			{
				double elem = 0;
				
				for(int k = 0, m = 0; k < m1.Width && m < m2.Height; k++, m++)
				{
					elem = elem + m1[i, k] * m2[m, j];
				}				
				
				rezmatrix[i, j] = elem;
			}
			
			return rezmatrix;
		}
		
		public static Matrix operator + (Matrix m1, Matrix m2)
		{
			Matrix rezmatrix;
			
			if (m1.Width != m2.Width)
				throw new Exception("Матрицы не совпадают по ширине");
			if (m1.Height != m2.Height)
				throw new Exception("Матрицы не совпадают по высоте");
			
			rezmatrix = new Matrix(m1.Width, m1.Height);
			
			for (int i = 0; i < rezmatrix.Height; i++)
				for (int j = 0; j < rezmatrix.Width; j++)					
			{
				rezmatrix[i,j] = m1[i,j] + m2[i,j];
			}			
			
			return rezmatrix;
		}

		public static Matrix operator - (Matrix matrix)
		{
			Matrix rezmatrix = new Matrix(matrix.Width, matrix.Height);
			
			for (int i = 0; i < rezmatrix.Height; i++)
				for (int j = 0; j < rezmatrix.Width; j++)					
			{
				rezmatrix[i,j] = -matrix[i,j];
			}			
			
			return rezmatrix;
		}
		
		public static Matrix operator - (Matrix m1, Matrix m2)
		{
			Matrix rezmatrix;
			
			if (m1.Width != m2.Width)
				throw new Exception("Матрицы не совпадают по ширине");
			if (m1.Height != m2.Height)
				throw new Exception("Матрицы не совпадают по высоте");
			
			rezmatrix = new Matrix(m1.Width, m1.Height);
			
			for (int i = 0; i < rezmatrix.Height; i++)
				for (int j = 0; j < rezmatrix.Width; j++)					
			{
				rezmatrix[i,j] = m1[i,j] - m2[i,j];
			}			
			
			return rezmatrix;
		}
	}
}

