﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImgModLib.Graphic
{
    public enum TransformType
    {
        General,
        Negative,
        Solarization,
        Logarithm,
        Contrast,
        Equalization,
        ChangeIntensity
    }

    public enum ColorSchema { RGB, CMY, XYZ, HSV, YUV, Lab }
    public enum OrdderFilterType { Minimum, Maximum, Median }

    public enum SpaceFilterType
    {
        /// <summary>
        /// Фильтр, заданный пользователем
        /// </summary>
        UserFilter,

        /// <summary>
        /// Усредняющий фильтр. Выделяет низкочатстоную чать изображения
        /// </summary>
        Averaging,

        /// <summary>
        /// Треугольный фильтр. Выделяет низкочатстоную чать изображения
        /// </summary>
        Triangle,

        /// <summary>
        /// Фильтр Гаусса. Выделяет низкочатстоную чать изображения
        /// </summary>
        Gaussian,

        /// <summary>
        /// Вертикальный фильтр Превита. Выделяет высокочатстоную чать изображения
        /// </summary>
        PrevitVertical,

        /// <summary>
        /// Горизонтальный фильтр Превита. Выделяет высокочатстоную чать изображения
        /// </summary>
        PrevitHorizontal,

        /// <summary>
        /// Вертикальный фильтр Собеля. Выделяет высокочатстоную чать изображения
        /// </summary>
        SobelVertical,

        /// <summary>
        /// Горизонтальный фильтр Собеля. Выделяет высокочатстоную чать изображения
        /// </summary>
        SobelHorizontal,

        /// <summary>
        /// Высокочастотный фильтр. Выделяет высокочатстоную чать изображения
        /// </summary>
        HighFrequency,

        /// <summary>
        /// Фильтр поднятия высоких частот. Повышает резкость изображения
        /// </summary>
        UpperHighFrequency,

        /// <summary>
        /// Нерезкое маскирование. Повышает резкость изображения
        /// </summary>
        Diffusing
    }
}
