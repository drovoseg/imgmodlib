﻿using System;
using System.Linq;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using ImgModLib.Mathematic;
using System.IO;


namespace ImgModLib.Graphic
{
    public sealed class ImageEx : ICloneable
    {
        public class ColorPlanes
        {
            ColorPlane[] planes = null;
            ImageEx parentimage;

            ColorPlanes()
            {
                planes = null;                
            }

            internal ColorPlanes(int number, ImageEx parentimage)
            {
                planes = new ColorPlane[number];
                this.parentimage = parentimage;
            }

            public ColorPlane this[int index]
            {
                get { return planes[index - 1]; }
                set 
                {
                    if (value.Width != parentimage.width || value.Height != parentimage.Height)
                        throw new InvalidOperationException("Несвопадение размеров присваиваемой плоскости и изображения");

                    if (planes[index - 1] != null) 
                        value.PlaneName = planes[index - 1].PlaneName;
                    planes[index - 1] = value;                    
                    planes[index - 1].Image = parentimage;                    

                    if (index - 1 == 0)
                        parentimage.plane1 = planes[index - 1];
                    else if (index - 1 == 1)
                        parentimage.plane2 = planes[index - 1];
                    else
                        parentimage.plane3 = planes[index - 1];

                    parentimage.NeedUpdate = true;
                }
            }

            public ColorPlane this[string name]
            {
                get
                {
                    ColorPlane needplane = planes.FirstOrDefault(plane => plane.PlaneName == name);

                    if (needplane == null)
                        throw new Exception("Изображение не содержит плоскости с указанным цветовым каналом");

                    return needplane;
                }

                set
                {
                    if (value.Width != parentimage.width || value.Height != parentimage.Height)
                        throw new InvalidOperationException("Несвопадение размеров присваиваемой плоскости и изображения");

                    ColorPlane needplane = planes.FirstOrDefault(plane => plane.PlaneName == name);
                    for (int i = 0; i < planes.Length; i++)
                    {
                        if (planes[i].PlaneName == name)
                        {
                            planes[i] = value;
                            planes[i].Image = parentimage;
                            planes[i].PlaneName = name;

                            if (i == 0)
                                parentimage.plane1 = planes[i];
                            else if (i == 1)
                                parentimage.plane2 = planes[i];
                            else
                                parentimage.plane3 = planes[i];

                            parentimage.NeedUpdate = true;

                            return;
                        }
                    }

                    throw new Exception("Изображение не содержит плоскости с указанным цветовым каналом");
                }
            }
        }

        public const double NORMA = byte.MaxValue;

        static readonly IEnumerable<PixelFormat> SUPPORTED_PIXELFORMATS = new HashSet<PixelFormat>()
        {
        	PixelFormat.Format8bppIndexed,
            PixelFormat.Format16bppArgb1555,
            PixelFormat.Format16bppRgb555,
            PixelFormat.Format16bppRgb565,
            PixelFormat.Format24bppRgb,
            PixelFormat.Format32bppArgb,
            PixelFormat.Format32bppRgb
        };

        static int GlobalCounter = 0;

        int counter;
        int height, width;
        ColorPlane plane1, plane2, plane3;
        ColorPlanes planes;
        List<Point> regionOfInterest = null;
        
        Bitmap bitmap = null;

        public List<Point> RegionOfInterest
        {
            get { return regionOfInterest; }
            set
            {
                if (value != null && value.Where(point => 
                    {
                        return point.X < 0 || point.X > width - 1 ||
                               point.Y < 0 || point.Y > height - 1;
                    }).Count() > 0)
                    throw new InvalidOperationException("Область интереса лежит вне изображения");

                regionOfInterest = value;
                plane1.RegionOfInterest = value;
                plane2.RegionOfInterest = value;
                plane3.RegionOfInterest = value;
            }
        }

        public ColorPlanes Planes { get { return planes; } }

        public bool NeedUpdate { get; set; }

        /// <summary>
        /// Высота изображения в пискелях
        /// </summary>
        public int Height 
        {
            get { return height; }
        }

        /// <summary>
        /// Ширина изображения в пикселях
        /// </summary>
        public int Width
        {
            get { return width; }
        }

        public Size ImageSize { get { return new Size(Width, Height); } }

        ColorSchema schema;

        public ColorSchema Schema 
        {
            get { return schema; }
            private set
            {
                schema = value;

                switch (schema)
                {
                    case ColorSchema.RGB:                        
                        plane1.PlaneName = "R";
                        plane2.PlaneName = "G";
                        plane3.PlaneName = "B";
                        break;

                    case ColorSchema.CMY:
                        plane1.PlaneName = "C";
                        plane2.PlaneName = "M";
                        plane3.PlaneName = "Y";
                        break;

                    case ColorSchema.XYZ:
                        plane1.PlaneName = "X";
                        plane2.PlaneName = "Y";
                        plane3.PlaneName = "Z";
                        break;

                    case ColorSchema.HSV:
                        plane1.PlaneName = "H";
                        plane2.PlaneName = "S";
                        plane3.PlaneName = "V";
                        break;

                    case ColorSchema.YUV:
                        plane1.PlaneName = "Y";
                        plane2.PlaneName = "U";
                        plane3.PlaneName = "V";
                        break;

                    case ColorSchema.Lab:
                        plane1.PlaneName = "L";
                        plane2.PlaneName = "a";
                        plane3.PlaneName = "b";
                        break;
                }
            }
        }
        
        public override string ToString()
        {
            return "ImageEx №" + counter;
        }

        private void InitComponents(int height, int width)
        {
            if (height <= 0)
                throw new InvalidOperationException("Высота изображения должна быть больше 0");
            if (width <= 0)
                throw new InvalidOperationException("Ширина изображения должна быть больше 0");

            this.height = height;
            this.width = width;

            plane1 = new ColorPlane(Height, Width, "R");            
            plane2 = new ColorPlane(Height, Width, "G");
            plane3 = new ColorPlane(Height, Width, "B");
            
            plane1.Image = this;
            plane2.Image = this;
            plane3.Image = this;

            planes[1] = plane1;
            planes[2] = plane2;
            planes[3] = plane3;

            Schema = ColorSchema.RGB;
        }

        static Bitmap CopyBitmap(Bitmap baseBitmap)
        {
            Bitmap rezBitmap;
            Graphics g;

            if (!SUPPORTED_PIXELFORMATS.Contains(baseBitmap.PixelFormat))
            {
                baseBitmap.Dispose();
                baseBitmap = null;
                GC.Collect();

                throw new InvalidDataException("Данный формат изображений не поддерживается");
            }

            rezBitmap = new Bitmap(baseBitmap.Width, baseBitmap.Height, PixelFormat.Format32bppArgb);

            g = Graphics.FromImage(rezBitmap);
            g.DrawImage(baseBitmap, 0, 0, baseBitmap.Width, baseBitmap.Height);
            g.Dispose();
            g = null;

            GC.Collect();

            return rezBitmap;
        }

        private ImageEx()
        {
            counter = GlobalCounter;
            GlobalCounter++;

            height = 0;
            width = 0;
            plane1 = plane2 = plane3 = null;
            planes = null;
            NeedUpdate = false;
            planes = new ColorPlanes(3, this);
        }

        /// <summary>
        /// Создает новый объект изображение указанного размера
        /// </summary>
        /// <param name="height">Высота изображения в пискелях</param>
        /// <param name="width">Ширина изображения в пикселях</param>
        public ImageEx(int height, int width) : this()
        {
            InitComponents(height, width);
            NeedUpdate = true;
        }

        public ImageEx(Bitmap basebitmap) : this()            
        {
            bitmap = CopyBitmap(basebitmap);
            InitComponents(basebitmap.Height, basebitmap.Width);
            LoadDataFromBitmap(basebitmap);
            
            NeedUpdate = false;
        }

        /// <summary>
        /// Создает новый объект изображение на основе рисунка, хранящегося на диске
        /// </summary>
        /// <param name="fileName">Имя файла рисунка на диске</param>
        public ImageEx(string fileName) : this()            
        {            
            Bitmap baseBitmap = new Bitmap( fileName );

            bitmap = CopyBitmap(baseBitmap);

            InitComponents(baseBitmap.Height, baseBitmap.Width);
            LoadDataFromBitmap(baseBitmap);

            baseBitmap.Dispose();
            baseBitmap = null;
            NeedUpdate = false;
            GC.Collect();
        }

        private void LoadDataFromBitmap(Bitmap bitmap)
        {
            ColorPlane RedPlane, GreenPlane, BluePlane;

            RedPlane = plane1;
            BluePlane = plane3;
            GreenPlane = plane2;

            BitmapData bitmapdata = bitmap.LockBits(new Rectangle(0, 0, Width, Height),
                                                    ImageLockMode.ReadWrite,
                                                    bitmap.PixelFormat);
            unsafe
            {
                byte R, G, B;
                ushort* ushort_ptr0;
                byte* byte_ptr0;

                switch (bitmap.PixelFormat)
                {
                    case PixelFormat.Format8bppIndexed:
                        ColorPalette palette = bitmap.Palette;

                        byte_ptr0 = (byte*)bitmapdata.Scan0;

                        for (int y = 0; y < Height; y++)
                        {
                            byte* ptr = byte_ptr0;

                            for (int x = 0; x < Width; x++)
                            {
                                Color tablecolor = palette.Entries[*ptr];

                                BluePlane[x, y] = tablecolor.B / NORMA;
                                GreenPlane[x, y] = tablecolor.G / NORMA;
                                RedPlane[x, y] = tablecolor.R / NORMA;
                                ptr++;
                            }
                            byte_ptr0 += bitmapdata.Stride;
                        }
                        break;

                    case PixelFormat.Format16bppArgb1555:
                    case PixelFormat.Format16bppRgb555:
                        ushort_ptr0 = (ushort*)bitmapdata.Scan0;

                        for (int y = 0; y < Height; y++)
                        {
                            ushort* ptr = ushort_ptr0;

                            for (int x = 0; x < Width; x++)
                            {
                                ushort val = *ptr;
                                B = (byte)((val & 0x001F) * 8);
                                G = (byte)(((val & 0x03E0) >> 5) * 8);
                                R = (byte)(((val & 0x7C00) >> 10) * 8);

                                BluePlane[x, y] = B / NORMA;
                                GreenPlane[x, y] = G / NORMA;
                                RedPlane[x, y] = R / NORMA;
                                ptr++;
                            }
                            ushort_ptr0 = (ushort*)((byte*)ushort_ptr0 + bitmapdata.Stride);
                        }
                        break;

                    case PixelFormat.Format16bppRgb565:
                        ushort_ptr0 = (ushort*)bitmapdata.Scan0;

                        for (int y = 0; y < Height; y++)
                        {
                            ushort* ptr = ushort_ptr0;

                            for (int x = 0; x < Width; x++)
                            {
                                ushort val = *ptr;
                                R = (byte)((val & 0x001F) * 8);
                                G = (byte)(((val & 0x05E0) >> 5) * 8);
                                B = (byte)(((val & 0x7800) >> 11) * 8);

                                BluePlane[x, y] = B / NORMA;
                                GreenPlane[x, y] = G / NORMA;
                                RedPlane[x, y] = R / NORMA;
                                ptr++;
                            }
                            ushort_ptr0 = (ushort*)((byte*)ushort_ptr0 + bitmapdata.Stride);
                        }
                        break;

                    case PixelFormat.Format24bppRgb:
                        byte_ptr0 = (byte*)bitmapdata.Scan0;

                        for (int y = 0; y < Height; y++)
                        {
                            byte* ptr = byte_ptr0;

                            for (int x = 0; x < Width; x++)
                            {
                                BluePlane[x, y] = (*ptr) / NORMA; ptr++;
                                GreenPlane[x, y] = (*ptr) / NORMA; ptr++;
                                RedPlane[x, y] = (*ptr) / NORMA; ptr++;
                            }
                            byte_ptr0 += bitmapdata.Stride;
                        }
                        break;

                    case PixelFormat.Format32bppArgb:
                    case PixelFormat.Format32bppRgb:
                        byte_ptr0 = (byte*)bitmapdata.Scan0;

                        for (int y = 0; y < Height; y++)
                        {
                            byte* ptr = byte_ptr0;

                            for (int x = 0; x < Width; x++)
                            {
                                BluePlane[x, y] = (*ptr) / NORMA; ptr++;
                                GreenPlane[x, y] = (*ptr) / NORMA; ptr++;
                                RedPlane[x, y] = (*ptr) / NORMA; ptr++;
                                ptr++;
                            }
                            byte_ptr0 += bitmapdata.Stride;
                        }
                        break;
                }
            }

            bitmap.UnlockBits(bitmapdata);
            bitmapdata = null;
            GC.Collect();
        }

        private void PutDownDataToBitmap(Bitmap bitmap)
        {
            ColorPlane RedPlane = Planes["R"];
            ColorPlane BluePlane = Planes["B"];
            ColorPlane GreenPlane = Planes["G"];

            BitmapData bitmapdata = bitmap.LockBits(new Rectangle(0, 0, Width, Height),
                                                   ImageLockMode.ReadWrite,
                                                   PixelFormat.Format24bppRgb);
            unsafe
            {
                byte* ptr0 = (byte*)bitmapdata.Scan0;
                for (int y = 0; y < Height; y++)
                {
                    byte* ptr = ptr0;

                    for (int x = 0; x < Width; x++)
                    {
                        *ptr = (byte)(BluePlane[x, y] * NORMA); ptr++;
                        *ptr = (byte)(GreenPlane[x, y] * NORMA); ptr++;
                        *ptr = (byte)(RedPlane[x, y] * NORMA); ptr++;
                    }
                    ptr0 += bitmapdata.Stride;
                }
            }

            bitmap.UnlockBits(bitmapdata);

            bitmapdata = null;
            GC.Collect();
        }

        public void Eclipse()
        {
            plane1.Eclipse();
            plane2.Eclipse();
            plane3.Eclipse();
        }

        public void Update()
        {
            ImageEx copyimg = (ImageEx)this.Clone();

            bitmap = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);

            if (Schema != ColorSchema.RGB)
            {
                copyimg.ToRGB();
            }

            copyimg.Eclipse();
            copyimg.PutDownDataToBitmap(bitmap);
            copyimg = null;

            NeedUpdate = false;
            GC.Collect();
        }

        public void Draw(int drawX, int drawY, int drawWidth, int drawHeight, Graphics g)
        {
            if (NeedUpdate) Update();
            g.DrawImage(bitmap, drawX, drawY, drawWidth, drawHeight);
        }

        public void Draw(int srcX, int srcY, int srcWidth, int srcHeigth, int drawX, int drawY, int drawWidth, int drawHeight, Graphics g)
        {
            if (NeedUpdate) Update();

            g.DrawImage(bitmap,
                        new Rectangle(drawX, drawY, drawWidth, drawHeight), 
                        new Rectangle(srcX, srcY, srcWidth, srcHeigth),                         
                        GraphicsUnit.Pixel);
        }

        void PlaneTranslation(ColorPlane SrcPlane1, ColorPlane SrcPlane2, ColorPlane SrcPlane3,
                              ColorPlane DestPlane1, ColorPlane DestPlane2, ColorPlane DestPlane3,
                              Matrix Matrix)
        {
            int height = SrcPlane1.Height;
            int width = SrcPlane1.Width;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double src1 = SrcPlane1[x, y];
                    double src2 = SrcPlane2[x, y];
                    double src3 = SrcPlane3[x, y];

                    double rez1 = Matrix[0, 0] * src1 + Matrix[0, 1] * src2 + Matrix[0, 2] * src3;
                    double rez2 = Matrix[1, 0] * src1 + Matrix[1, 1] * src2 + Matrix[1, 2] * src3;
                    double rez3 = Matrix[2, 0] * src1 + Matrix[2, 1] * src2 + Matrix[2, 2] * src3;

                    DestPlane1[x, y] = rez1;
                    DestPlane2[x, y] = rez2;
                    DestPlane3[x, y] = rez3;
                }
            }
        }

        public void ToRGB()
        {
            if (Schema == ColorSchema.RGB) return;

            ColorPlane RPlane, GPlane, BPlane;
            int width, height;
            Matrix matrix;

            if (Schema == ColorSchema.Lab)
            {
                ToXYZ();
                ToRGB();
                return;
            }

            height = Planes[1].Height;
            width = Planes[1].Width;

            RPlane = new ColorPlane(height, width, "R");
            GPlane = new ColorPlane(height, width, "G");
            BPlane = new ColorPlane(height, width, "B");

            switch (Schema)
            {
                case ColorSchema.CMY:                    
                    ColorPlane CPlane, MPlane, YPlane;

                    CPlane = Planes["C"];
                    MPlane = Planes["M"];
                    YPlane = Planes["Y"];

                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            RPlane[x, y] = 1 - CPlane[x, y];
                            GPlane[x, y] = 1 - MPlane[x, y];
                            BPlane[x, y] = 1 - YPlane[x, y];
                        }
                    }                    
                    break;

                case ColorSchema.XYZ:
                    matrix = Support.XYZ_Matrix.GetReverseMatrix();

                    PlaneTranslation(Planes["X"], Planes["Y"], Planes["Z"],
                                     RPlane, GPlane, BPlane, matrix);
                    break;

                case ColorSchema.YUV:
                    matrix = Support.YUV_Matrix.GetReverseMatrix();

                    PlaneTranslation(Planes["Y"], Planes["U"], Planes["V"],
                                     RPlane, GPlane, BPlane, matrix);
                    break;

                case ColorSchema.HSV:
                    ColorPlane HPlane, SPlane, VPlane;

                    HPlane = Planes["H"];
                    SPlane = Planes["S"];
                    VPlane = Planes["V"];

                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            double R = 0, G = 0, B = 0, H, S, V;

                            H = HPlane[x, y] * ImageEx.NORMA;
                            S = SPlane[x, y] * ImageEx.NORMA / 100d;
                            V = VPlane[x, y] * ImageEx.NORMA;

                            if (S <= 0 || double.IsNaN(H)) R = G = B = V;
                            else
                            {
                                int sector = (int)Math.Floor(H / 60);
                                if (sector < 0) sector = 0;
                                if (sector > 5) sector = 5;
                                double frac = H / 60 - sector;

                                double T = V * (1 - S);
                                double P = V * (1 - S * frac);
                                double Q = V * (1 - S * (1 - frac));

                                switch (sector)
                                {
                                    case 0: R = V; G = Q; B = T; break;
                                    case 1: R = P; G = V; B = T; break;
                                    case 2: R = T; G = V; B = Q; break;
                                    case 3: R = T; G = P; B = V; break;
                                    case 4: R = Q; G = T; B = V; break;
                                    case 5: R = V; G = T; B = P; break;
                                }
                            }

                            RPlane[x, y] = R / ImageEx.NORMA;
                            GPlane[x, y] = G / ImageEx.NORMA;
                            BPlane[x, y] = B / ImageEx.NORMA;
                        }
                    }
                    break;
            }

            Schema = ColorSchema.RGB;

            Planes["R"] = RPlane;
            Planes["G"] = GPlane;
            Planes["B"] = BPlane;

            GC.Collect();
        }

        public void ToCMY()
        {
            if (Schema == ColorSchema.CMY) return;

            ColorPlane CPlane, MPlane, YPlane;
            ColorPlane RPlane, GPlane, BPlane;
            int width, height;

            if (Schema != ColorSchema.RGB)
            {
                ToRGB();
                ToCMY();
                return;
            }

            height = Planes[1].Height;
            width = Planes[1].Width;
            
            CPlane = new ColorPlane(height, width, "C");
            MPlane = new ColorPlane(height, width, "M");
            YPlane = new ColorPlane(height, width, "Y");

            RPlane = Planes["R"];
            GPlane = Planes["G"];
            BPlane = Planes["B"];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    CPlane[x, y] = 1 - RPlane[x, y];
                    MPlane[x, y] = 1 - GPlane[x, y];
                    YPlane[x, y] = 1 - BPlane[x, y];
                }
            }

            Schema = ColorSchema.CMY;

            Planes["C"] = CPlane;
            Planes["M"] = MPlane;
            Planes["Y"] = YPlane;

            GC.Collect();
        }

        public void ToXYZ()
        {
            if (Schema == ColorSchema.XYZ) return;
                        
            ColorPlane XPlane, YPlane, ZPlane;
            int width, height;
            Matrix matrix;

            if (Schema == ColorSchema.CMY || Schema == ColorSchema.HSV)
            {
                ToRGB();
                ToXYZ();
                return;
            }

            height = Planes[1].Height;
            width = Planes[1].Width;

            XPlane = new ColorPlane(height, width, "X");
            YPlane = new ColorPlane(height, width, "Y");
            ZPlane = new ColorPlane(height, width, "Z");

            switch (Schema)
            {
                case ColorSchema.RGB:
                    matrix = Support.XYZ_Matrix;

                    PlaneTranslation(Planes["R"], Planes["G"], Planes["B"],
                                     XPlane, YPlane, ZPlane, matrix);
                    break;                    

                case ColorSchema.YUV:
                    matrix = Support.XYZ_Matrix * Support.YUV_Matrix.GetReverseMatrix();

                    PlaneTranslation(Planes["Y"], Planes["U"], Planes["V"],
                                     XPlane, YPlane, ZPlane, matrix);                    
                    break;

                case ColorSchema.Lab:
                    ColorPlane LPlane, aPlane, bPlane;
                    
                    double XMaxVal = Support.Xw;
                    double YMaxVal = Support.Yw;
                    double ZMaxVal = Support.Zw;

                    LPlane = Planes["L"];
                    aPlane = Planes["a"];
                    bPlane = Planes["b"];

                    for (int y = 0; y < height; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            double L = LPlane[x, y];
                            double a = aPlane[x, y];
                            double b = bPlane[x, y];

                            double fy = (L + Support.C_16) / Support.C_116;
                            double fx = a / Support.C_500 + fy;
                            double fz = fy - b / Support.C_200;

                            double X = Support.t(fx) * XMaxVal;
                            double Y = Support.t(fy) * YMaxVal;
                            double Z = Support.t(fz) * ZMaxVal;

                            XPlane[x, y] = X;
                            YPlane[x, y] = Y;
                            ZPlane[x, y] = Z;
                        }
                    }
                    break;
            }

            Schema = ColorSchema.XYZ;

            Planes["X"] = XPlane;
            Planes["Y"] = YPlane;
            Planes["Z"] = ZPlane;

            GC.Collect();
        }

        public void ToYUV()
        {
            if (Schema == ColorSchema.YUV) return;

            ColorPlane YPlane, UPlane, VPlane;
            int width, height;
            Matrix matrix;

            if (Schema != ColorSchema.RGB && Schema != ColorSchema.XYZ)
            {
                ToRGB();
                ToYUV();
                return;
            }

            height = Planes[1].Height;
            width = Planes[1].Width;

            YPlane = new ColorPlane(height, width, "Y");
            UPlane = new ColorPlane(height, width, "U");
            VPlane = new ColorPlane(height, width, "V");

            switch (Schema)
            {
                case ColorSchema.RGB:
                    matrix = Support.YUV_Matrix;

                    PlaneTranslation(Planes["R"], Planes["G"], Planes["B"],
                                     YPlane, UPlane, VPlane, matrix);
                    break;

                case ColorSchema.XYZ:
                    matrix = Support.YUV_Matrix * Support.XYZ_Matrix.GetReverseMatrix();

                    PlaneTranslation(Planes["X"], Planes["Y"], Planes["Z"],
                                     YPlane, UPlane, VPlane, matrix);
                    break;
            }

            Schema = ColorSchema.YUV;

            Planes["Y"] = YPlane;
            Planes["U"] = UPlane;
            Planes["V"] = VPlane;

            GC.Collect();
        }

        public void ToHSV()
        {
            if (Schema == ColorSchema.HSV) return;

            ColorPlane HPlane, SPlane, VPlane;
            ColorPlane RPlane, GPlane, BPlane;
            int width, height;

            if (Schema != ColorSchema.RGB)
            {
                ToRGB();
                ToHSV();
                return;
            }

            height = Planes[1].Height;
            width = Planes[1].Width;

            HPlane = new ColorPlane(height, width, "H");
            SPlane = new ColorPlane(height, width, "S");
            VPlane = new ColorPlane(height, width, "V");

            RPlane = Planes["R"];
            GPlane = Planes["G"];
            BPlane = Planes["B"];
            
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double R, G, B, H, S, V;
                    double min, max, dif;

                    R = RPlane[x, y] * ImageEx.NORMA;
                    G = GPlane[x, y] * ImageEx.NORMA;
                    B = BPlane[x, y] * ImageEx.NORMA;

                    max = Math.Max(Math.Max(R, G), B);
                    min = Math.Min(Math.Min(R, G), B);
                    dif = max - min;

                    V = max;

                    if (max == 0)
                        S = 0;
                    else
                        S = dif * 100 / max;

                    if (S == 0)
                        H = double.NaN;
                    else
                    {
                        if (R == max)
                            H = (G - B) / dif;
                        else if (G == max)
                            H = 2 + (B - R) / dif;
                        else 
                            H = 4 + (R - G) / dif;

                        H *= 60;

                        if (H < 0) H += 360;
                    }

                    HPlane[x, y] = H / ImageEx.NORMA;
                    SPlane[x, y] = S / ImageEx.NORMA;
                    VPlane[x, y] = V / ImageEx.NORMA;
                }
            }

            Schema = ColorSchema.HSV;

            Planes["H"] = HPlane;
            Planes["S"] = SPlane;
            Planes["V"] = VPlane;

            GC.Collect();
        }

        public void ToLab()
        {
            if (Schema == ColorSchema.Lab) return;

            ColorPlane LPlane, aPlane, bPlane;
            ColorPlane XPlane, YPlane, ZPlane;
            int width, height;

            if (Schema != ColorSchema.XYZ)
            {
                ToXYZ();
                ToLab();
                return;
            }

            height = Planes[1].Height;
            width = Planes[1].Width;

            LPlane = new ColorPlane(height, width, "L");
            aPlane = new ColorPlane(height, width, "a");
            bPlane = new ColorPlane(height, width, "b");

            XPlane = Planes["X"];
            YPlane = Planes["Y"];
            ZPlane = Planes["Z"];

            double XMaxVal = Support.Xw;
            double YMaxVal = Support.Yw;
            double ZMaxVal = Support.Zw;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double X = XPlane[x, y];
                    double Y = YPlane[x, y];
                    double Z = ZPlane[x, y];

                    double fx = Support.f(X / XMaxVal);
                    double fy = Support.f(Y / YMaxVal);
                    double fz = Support.f(Z / ZMaxVal);

                    double L = Support.C_116 * fy - Support.C_16;
                    double a = Support.C_500 * (fx - fy);
                    double b = Support.C_200 * (fy - fz);

                    LPlane[x, y] = L;
                    aPlane[x, y] = a;
                    bPlane[x, y] = b;
                }
            }

            Schema = ColorSchema.Lab;

            Planes["L"] = LPlane;
            Planes["a"] = aPlane;
            Planes["b"] = bPlane;

            GC.Collect();
        }

        public void Save(string filename, ImageFormat imageformat)
        {
            if (NeedUpdate) Update();

            try
            {
                bitmap.Save(filename, imageformat);
            }
            catch (ExternalException)
            {
            	File.Delete(filename);
            	bitmap.Save(filename, imageformat);
            }
        }

        public void Save(string filename)
        {
            Save(filename, ImageFormat.Bmp);
        }

        #region Члены ICloneable

        public object Clone()
        {
            ImageEx copyimage = new ImageEx();

            copyimage.height = this.height;
            copyimage.width = this.width;            
            if (bitmap != null) copyimage.bitmap = (Bitmap)this.bitmap.Clone();            

            copyimage.plane1 = (ColorPlane)this.plane1.Clone();
            copyimage.plane2 = (ColorPlane)this.plane2.Clone();
            copyimage.plane3 = (ColorPlane)this.plane3.Clone();

            copyimage.planes = new ColorPlanes(3, copyimage);
            copyimage.planes[1] = copyimage.plane1;
            copyimage.planes[2] = copyimage.plane2;
            copyimage.planes[3] = copyimage.plane3;

            copyimage.Schema = this.Schema;
            copyimage.NeedUpdate = this.NeedUpdate;
            copyimage.RegionOfInterest = RegionOfInterest;

            return copyimage;
        }

        #endregion

        static ImageEx GetImageTemplate(ImageEx image1, ImageEx image2)
        {
            ImageEx RezImage = null;

            if (image1.Schema != image2.Schema)
                throw new InvalidOperationException("Изображения должны быть в одной цветовой схеме");

            RezImage = new ImageEx();
            RezImage.height = image1.height;
            RezImage.width = image1.width;

            return RezImage;
        }

        static ImageEx GetImageTemplate(ImageEx image)
        {
            ImageEx RezImage = new ImageEx();
            RezImage.height = image.height;
            RezImage.width = image.width;

            return RezImage;
        }

        public ImageEx PutBackground(ImageEx background, double imgPercent, double bkgPercent)
        {

            if (background == null)
                throw new InvalidOperationException("Фоновое изображение не задано");

            if (imgPercent > 1 || imgPercent < 0 || bkgPercent > 1 || bkgPercent < 0)
                throw new InvalidOperationException("Значение пропорциональных коэффициентов должны лежать в диапазоне [0; 1]");

            if (imgPercent + bkgPercent != 1)
                throw new InvalidOperationException("Не выполняется условие imgPercent+bkgPercent=1");

            ImageEx result = this * imgPercent + background * bkgPercent;

            result.NeedUpdate = true;
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plane1"></param>
        /// <param name="plane"></param>
        /// <returns></returns>
        public static ImageEx operator +(ImageEx image1, ImageEx image2)
        {
            ImageEx RezImage = GetImageTemplate(image1, image2);

            RezImage.Planes[1] = image1.Planes[1].Add(image2.Planes[1]);
            RezImage.Planes[2] = image1.Planes[2].Add(image2.Planes[2]);
            RezImage.Planes[3] = image1.Planes[3].Add(image2.Planes[3]);

            RezImage.Schema = image1.Schema;
            RezImage.NeedUpdate = true;

            return RezImage;
        }


        public static ImageEx operator +(ImageEx image, double k)
        {
            ImageEx RezImage = GetImageTemplate(image); 

            RezImage.Planes[1] = image.Planes[1].Add(k);
            RezImage.Planes[2] = image.Planes[2].Add(k);
            RezImage.Planes[3] = image.Planes[3].Add(k);

            RezImage.Schema = image.Schema;
            RezImage.NeedUpdate = true;

            return RezImage;
        }

        public static ImageEx operator +(double k, ImageEx image)
        {
            return image + k;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plane1"></param>
        /// <param name="plane"></param>
        /// <returns></returns>
        public static ImageEx operator -(ImageEx image1, ImageEx image2)
        {
            ImageEx RezImage = GetImageTemplate(image1, image2);

            RezImage.Planes[1] = image1.Planes[1].Sub(image2.Planes[1]);
            RezImage.Planes[2] = image1.Planes[2].Sub(image2.Planes[2]);
            RezImage.Planes[3] = image1.Planes[3].Sub(image2.Planes[3]);

            RezImage.Schema = image1.Schema;
            RezImage.NeedUpdate = true;

            return RezImage;
        }

        public static ImageEx operator -(ImageEx image)
        {
            return -1 * image;
        }

        public static ImageEx operator -(ImageEx image, double k)
        {
            ImageEx RezImage = GetImageTemplate(image); 

            RezImage.Planes[1] = image.Planes[1].Sub(k, false);
            RezImage.Planes[2] = image.Planes[2].Sub(k, false);
            RezImage.Planes[3] = image.Planes[3].Sub(k, false);

            RezImage.Schema = image.Schema;
            RezImage.NeedUpdate = true;

            return RezImage;
        }

        public static ImageEx operator -(double k, ImageEx image)
        {
            return image - k;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plane1"></param>
        /// <param name="plane"></param>
        /// <returns></returns>
        public static ImageEx operator *(ImageEx image1, ImageEx image2)
        {
            ImageEx RezImage = GetImageTemplate(image1, image2);

            RezImage.Planes[1] = image1.Planes[1].Mul(image2.Planes[1]);
            RezImage.Planes[2] = image1.Planes[2].Mul(image2.Planes[2]);
            RezImage.Planes[3] = image1.Planes[3].Mul(image2.Planes[3]);

            RezImage.Schema = image1.Schema;
            RezImage.NeedUpdate = true;

            return RezImage;
        }

        public static ImageEx operator *(ImageEx image, double k)
        {
            ImageEx RezImage = GetImageTemplate(image); 

            RezImage.Planes[1] =  image.Planes[1].Mul(k);
            RezImage.Planes[2] = image.Planes[2].Mul(k);
            RezImage.Planes[3] = image.Planes[3].Mul(k);

            RezImage.Schema = image.Schema;
            RezImage.NeedUpdate = true;

            return RezImage;
        }

        public static ImageEx operator *(double k, ImageEx image)
        {
            return image * k;
        }

        public void ToColorSchema(ColorSchema s)
        {
            switch (s)
            {
                case ColorSchema.RGB: ToRGB(); break;
                case ColorSchema.CMY: ToCMY(); break;
                case ColorSchema.XYZ: ToXYZ(); break;
                case ColorSchema.YUV: ToYUV(); break;
                case ColorSchema.HSV: ToHSV(); break;
                case ColorSchema.Lab: ToLab(); break;
            }
        }

        public void ApplyGradTransform(GradationTransform gradtransf1,
                                       GradationTransform gradtransf2,
                                       GradationTransform gradtransf3)
        {
            ColorSchema s = Schema;
            ToRGB();

            if (gradtransf1 != null) plane1.ApplyGradTransform(gradtransf1);
            if (gradtransf2 != null) plane2.ApplyGradTransform(gradtransf2);
            if (gradtransf3 != null) plane3.ApplyGradTransform(gradtransf3);
            
            ToColorSchema(s);
            
            NeedUpdate = true;
        }

        public void ApplyGradTransform(GradationTransform gradtransf)
        {
            ApplyGradTransform(gradtransf, gradtransf, gradtransf);
        }

        public void ApplyFilter(SpaceFilterMasck masck1,
                                SpaceFilterMasck masck2,
                                SpaceFilterMasck masck3)
        {
            ColorSchema s = Schema;
            ToRGB();

            if (masck1 != null) plane1.ApplyFilter(masck1);
            if (masck2 != null) plane2.ApplyFilter(masck2);
            if (masck3 != null) plane3.ApplyFilter(masck3);

            ToColorSchema(s);

            NeedUpdate = true;
        }

        public void ApplyFilter(SpaceFilterMasck masck)
        {
            ApplyFilter(masck, masck, masck);
        }

        public void ApplyFilter(OrderFilterMasck masck1, OrderFilterMasck masck2, OrderFilterMasck masck3,
                                OrdderFilterType type1, OrdderFilterType type2, OrdderFilterType type3)
        {
            ColorSchema s = Schema;
            ToRGB();

            if (masck1 != null) plane1.ApplyFilter(masck1, type1);
            if (masck2 != null) plane2.ApplyFilter(masck2, type2);
            if (masck3 != null) plane3.ApplyFilter(masck3, type3);

            ToColorSchema(s);

            NeedUpdate = true;
        }

        public void ApplyFilter(OrderFilterMasck masck, OrdderFilterType type)
        {
            ApplyFilter(masck, masck, masck, type, type, type);
        }

        public void ApplyFilter(OrderFilterMasck masck1, OrderFilterMasck masck2, OrderFilterMasck masck3,
                                OrdderFilterType type)
        {
            ApplyFilter(masck1, masck2, masck3, type, type, type);
        }

        public static explicit operator Image(ImageEx imageex)
        {
            
        	if (imageex.NeedUpdate) imageex.Update();

            return imageex.bitmap;
        }
    }
}
 