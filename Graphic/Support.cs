﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ImgModLib.Mathematic;

namespace ImgModLib.Graphic
{
    static internal class Support
    {
        static internal readonly Matrix XYZ_Matrix = new Matrix(new double[,]
        {
            { 0.431, 0.342, 0.178 },
            { 0.222, 0.707, 0.071 },
            { 0.020, 0.130, 0.939 }
        });

        static internal readonly Matrix YUV_Matrix = new Matrix(new double[,]
        {
            { 0.299, 0.587, 0.114 },
            { -0.147, -0.289, 0.436 },
            { 0.615, 0.515, 0.1 }
        });

        internal const double POW = 3;
        internal const double C_116 = 116;
        internal const double C_500 = 500;
        internal const double C_200 = 200;
        internal const double C_16 = 16;
        internal const double PHI = 7.787;
        internal const double BORDER_T = 0.008856;
        internal const double BORDER_F = PHI * BORDER_T + C_16 / C_116;
        static internal readonly double Xw = XYZ_Matrix[0, 0] + XYZ_Matrix[0, 1] + XYZ_Matrix[0, 2];
        static internal readonly double Yw = XYZ_Matrix[1, 0] + XYZ_Matrix[1, 1] + XYZ_Matrix[1, 2];
        static internal readonly double Zw = XYZ_Matrix[2, 0] + XYZ_Matrix[2, 1] + XYZ_Matrix[2, 2];

        internal static double f(double t)
        {
            return (t > BORDER_T) ? Math.Pow(t, 1 / POW) : PHI * t + C_16 / C_116;
        }

        internal static double t(double f)
        {
            return (f > BORDER_F) ? Math.Pow(f, POW) : (f - C_16 / C_116) / PHI;
        }
    }
}
