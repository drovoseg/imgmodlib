﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImgModLib.Graphic
{
    static class Executor
    {
        static ImageEx HomomorphicProcessing(ImageEx image)
        {
            if (image == null) throw new Exception("Изображение не задано");

            SpaceFilterMasck masck = new SpaceFilterMasck(SpaceFilterType.Diffusing, 9, 0.5);
            ImageEx rez = (ImageEx)image.Clone();
            ColorPlane RPlane, GPlane, BPlane;
            int height = rez.Height, width = rez.Width;

            rez.ToRGB();
            rez.Eclipse();

            RPlane = rez.Planes["R"];
            GPlane = rez.Planes["G"];
            BPlane = rez.Planes["B"];

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    RPlane[x, y] = Math.Log(RPlane[x, y]);
                    GPlane[x, y] = Math.Log(GPlane[x, y]);
                    BPlane[x, y] = Math.Log(BPlane[x, y]);
                }
            }

            rez.ApplyFilter(masck);

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    RPlane[x, y] = Math.Exp(RPlane[x, y]);
                    GPlane[x, y] = Math.Exp(GPlane[x, y]);
                    BPlane[x, y] = Math.Exp(BPlane[x, y]);
                }
            }

            rez.NeedUpdate = true;            
            return rez;
        }
    }
}
