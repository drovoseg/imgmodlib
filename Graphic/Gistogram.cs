﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;

namespace ImgModLib.Graphic
{
    public class Gistogram
    {
        const int DEFAULT_HEIGHT = 500;

        double[] GistogramArr = null;        
        double MaxVal = 0;
        double upperLimit, lowerLimit;
        ulong NumPixels = 0;
        Bitmap bitmap = null;
        bool needUpdate = false;

        public int Count { get { return GistogramArr.Length; } }

        public double this[double brightnesLevel]
        {
            get 
            {
                int index = (int)((brightnesLevel - lowerLimit) * ImageEx.NORMA);
                return GistogramArr[index]; 
            }
            private set 
            {
                int index = (int)((brightnesLevel - lowerLimit) * ImageEx.NORMA);

                GistogramArr[index] = value;
                needUpdate = true;
            }
        }

        internal double this[int index]
        {
            get
            {                
                return GistogramArr[index];
            }
        }

        Color gistcolor;
        public Color GistogramColor
        {
            get { return gistcolor; }
            set 
            { 
                gistcolor = value;
                Update();
            }
        }

        double[] CreateGistogramArr(ColorPlane plane)
        {
            return new double[(int)((plane.MaxValue - plane.MinValue) * ImageEx.NORMA) + 1];
        }

        public Gistogram(ColorPlane plane, Color color)
        {
            ColorPlane copyplane = (ColorPlane)plane.Clone();

            lowerLimit = plane.MinValue;
            upperLimit = plane.MaxValue;            
            gistcolor = color;

            copyplane.Eclipse(false);
            NumPixels = (ulong)(copyplane.Height * copyplane.Width);
            
            GistogramArr = CreateGistogramArr(copyplane);                       
                        
            for (int y = 0; y < copyplane.Height; y++)
            {
                for (int x = 0; x < copyplane.Width; x++)
                {
                    this[copyplane[x, y]]++;
                }
            }

            Update();
                        
            copyplane = null;
            GC.Collect();
        }

        public void Update()
        {
            Graphics graph;
            Pen pen = new Pen(gistcolor);
            double scale;

            for (int i = 0; i < GistogramArr.Length; i++)
            {
                GistogramArr[i] /= NumPixels;

                if (MaxVal < GistogramArr[i]) MaxVal = GistogramArr[i];
            }

            scale = DEFAULT_HEIGHT / MaxVal;

            bitmap = new Bitmap(GistogramArr.Length + 1, DEFAULT_HEIGHT);
            graph = Graphics.FromImage(bitmap);

            for (int x = 0; x < GistogramArr.Length; x++)
            {
                float y = (float)(GistogramArr[x] * scale);

                graph.DrawLine(pen, x, bitmap.Height - 1, x, bitmap.Height - y - 1);
            }

            needUpdate = false;

            graph.Dispose();
            graph = null;
            GC.Collect();
        }

        public void Draw(int drawX, int drawY, int drawWidth, int drawHeight, Graphics g)
        {
            if (needUpdate) Update();

            g.DrawImage(bitmap, drawX, drawY, drawWidth, drawHeight);

            GC.Collect();
        }
    }
}
