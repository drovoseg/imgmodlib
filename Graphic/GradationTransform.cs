﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImgModLib.Graphic
{
    public class GradationTransform
    {
        double[] array;
        TransformType type;

        internal double this[int index]
        {
            get { return array[index]; }
            set { array[index] = value; }
        }

        public double this[double brightnesLevel]
        {
            get
            {
                int index = (int)(brightnesLevel * ImageEx.NORMA);
                return array[index];
            }
            private set
            {
                int index = (int)(brightnesLevel * ImageEx.NORMA);
                array[index] = value;
            }
        }

        public int Count { get { return array.Length; } }

        public TransformType Type
        {
            get { return type; }
        }

        public GradationTransform() : this(TransformType.General) {}

        public GradationTransform(TransformType type)
        {
            this.type = type;

            switch (type)
            {
                case TransformType.General: array = General(); break;
                case TransformType.Negative: array = Negative(); break;
                case TransformType.Logarithm: array = Logarithm(); break;
                case TransformType.Contrast: array = Conrtast(); break;
                case TransformType.Solarization: array = Solarization(); break;
                    
                case TransformType.Equalization: 
                    throw new InvalidOperationException("Для эквализации применяйте другой конструктор");
                case TransformType.ChangeIntensity:
                    throw new InvalidOperationException("Для линейного изменения яркости применяйте другой конструктор");
            }
        }

        public GradationTransform(double val)
        {
            type = TransformType.ChangeIntensity;

            array = ChangeIntensity(val);
        }

        public GradationTransform(Gistogram gistogram)
        {
            type = TransformType.Equalization;

            array = Equalization(gistogram);
        }

        double[] General()
        {
            double[] rez = new double[(int)(ImageEx.NORMA + 1)];

            for (int x = 0; x < rez.Length; x++)
            {
                rez[x] = x / ImageEx.NORMA;
            }

            return rez;
        }

        double[] Negative()
        {
            double[] rez = new double[(int)(ImageEx.NORMA + 1)];

            for (int x = 0; x < rez.Length; x++)
            {
                rez[x] = 1 - x / ImageEx.NORMA;
            }

            return rez;
        }

        double[] Solarization()
        {
            double[] rez = new double[(int)(ImageEx.NORMA + 1)];
            double extr = (ImageEx.NORMA + 1) / 2;

            for (int x = 0; x < rez.Length; x++)
            {
                rez[x] = -(x - extr) * (x - extr) / extr + extr;
                rez[x] /= ImageEx.NORMA;
            }

            return rez;
        }

        double[] ChangeIntensity(double k)
        {
            double[] rez = new double[(int)(ImageEx.NORMA + 1)];

            for (int x = 0; x < rez.Length; x++)
            {
                rez[x] = x / ImageEx.NORMA + k;

                if (rez[x] > 1) rez[x] = 1;
                if (rez[x] < 0) rez[x] = 0;
            }
            
            return rez;
        }

        double[] Logarithm()
        {
            double[] rez = new double[(int)(ImageEx.NORMA + 1)];
            
            for (int x = 0; x < rez.Length; x++)
            {
                rez[x] = Math.Log(x, ImageEx.NORMA);
            }

            rez[0] = 0;

            return rez;
        }        

        double[] Conrtast()
        {
            double[] rez = new double[(int)(ImageEx.NORMA + 1)];
            double boundval = (ImageEx.NORMA + 1) / 2;
            double N = ImageEx.NORMA + 1;
            double boundlog = Math.Log(boundval);

            for (int x = 0; x < rez.Length; x++)
            {
                if (x < boundval)
                    rez[x] = Math.Exp(boundlog * x / boundval) - 1;
                else
                    rez[x] = N - Math.Exp(-boundlog * (x - N) / boundval);

                rez[x] /= ImageEx.NORMA;
            }

            return rez;
        }

        double[] Equalization(Gistogram gist)
        {
            double[] rez = new double[(int)(ImageEx.NORMA + 1)];

            rez[0] = 0;

            for (int x = 1; x < rez.Length; x++)
            {
                rez[x] = rez[x - 1] + gist[x - 1];
            }

            return rez;
        }
    }
}
