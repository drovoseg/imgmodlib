﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using ImgModLib.Mathematic;

namespace ImgModLib.Graphic
{
    /// <summary>
    /// Представляет собой отдельный цветовой канал изображения
    /// </summary>
    public sealed class ColorPlane : ICloneable
    {
        sealed class BoundaryValues
        {
            double min, max;

            public double Min
            {
                get { return min; }
            }

            public double Max
            {
                get { return max; }
            }

            public BoundaryValues(double min, double max)
            {
                this.min = min;
                this.max = max;
            }
        }

        static int GlobalCounter = 0;
        static Dictionary<ColorSchema, Dictionary<string, BoundaryValues>> boundaryValues =
            new Dictionary<ColorSchema, Dictionary<string, BoundaryValues>>();

        static ColorPlane()
        {
            Dictionary<string, BoundaryValues> bounds = null;
            Matrix matrix;

            #region RGB

            bounds = new Dictionary<string, BoundaryValues>();

            bounds.Add("R", new BoundaryValues(0, 1));
            bounds.Add("G", new BoundaryValues(0, 1));
            bounds.Add("B", new BoundaryValues(0, 1));

            boundaryValues.Add(ColorSchema.RGB, bounds);
			#endregion            
            #region CMY

            bounds = new Dictionary<string, BoundaryValues>();

            bounds.Add("С", new BoundaryValues(0, 1));
            bounds.Add("M", new BoundaryValues(0, 1));
            bounds.Add("Y", new BoundaryValues(0, 1));

            boundaryValues.Add(ColorSchema.CMY, bounds);
			#endregion            
            #region XYZ

            matrix = Support.XYZ_Matrix;
            bounds = new Dictionary<string, BoundaryValues>();

            bounds.Add("X", new BoundaryValues(0, matrix[0, 0] + matrix[0, 1] + matrix[0, 2]));
            bounds.Add("Y", new BoundaryValues(0, matrix[1, 0] + matrix[1, 1] + matrix[1, 2]));
            bounds.Add("Z", new BoundaryValues(0, matrix[2, 0] + matrix[2, 1] + matrix[2, 2]));

            boundaryValues.Add(ColorSchema.XYZ, bounds);
			#endregion            
            #region YUV

            matrix = Support.YUV_Matrix;
            bounds = new Dictionary<string, BoundaryValues>();

            bounds.Add("Y", new BoundaryValues(0, matrix[0, 0] + matrix[0, 1] + matrix[0, 2]));
            bounds.Add("U", new BoundaryValues(matrix[1, 0] + matrix[1, 1], matrix[1, 2]));
            bounds.Add("V", new BoundaryValues(0, matrix[2, 0] + matrix[2, 1] + matrix[2, 2]));

            boundaryValues.Add(ColorSchema.YUV, bounds);
			#endregion            
            #region Lab

            bounds = new Dictionary<string, BoundaryValues>();

            bounds.Add("L", new BoundaryValues(Support.C_116 * Support.f(0) - Support.C_16,
                Support.C_116 * Support.f(1) - Support.C_16));
            bounds.Add("a", new BoundaryValues(Support.C_500 * (Support.f(0) - Support.f(1)),
                Support.C_500 * (Support.f(1) - Support.f(0))));
            bounds.Add("b", new BoundaryValues(Support.C_200 * (Support.f(0) - Support.f(1)),
                Support.C_200 * (Support.f(1) - Support.f(0))));

            boundaryValues.Add(ColorSchema.Lab, bounds);
			#endregion            
            #region HSV
            bounds = new Dictionary<string, BoundaryValues>();

            bounds.Add("H", new BoundaryValues(0, 360 / ImageEx.NORMA));
            bounds.Add("S", new BoundaryValues(0, 100 / ImageEx.NORMA));
            bounds.Add("V", new BoundaryValues(0, 1));

            boundaryValues.Add(ColorSchema.HSV, bounds);
			#endregion            
        }

        int counter;
        double[,] array;
        int height, width;
        string planename;
        List<Point> regionOfInterest = null;

        public List<Point> RegionOfInterest
        {
            get { return regionOfInterest; }
            set
            {
                if (value != null && value.Where(point =>
                {
                    return point.X < 0 || point.X > width - 1 ||
                           point.Y < 0 || point.Y > height - 1;
                }).Count() > 0)
            		throw new InvalidOperationException("Значения вне допустимого диапазона");

                regionOfInterest = value;
            }
        }

        /// <summary>
        /// Высота цветовой плоскости в пискелях
        /// </summary>
        public int Height
        {
            get { return height; }
            private set
            {
	            if (value <= 0)
	                throw new ArgumentOutOfRangeException("Высота цветовой плоскости должна быть больше 0");
	            
	            height = value;
            }
        }

        /// <summary>
        /// Ширина цветовой плоскости в пикселях
        /// </summary>
        public int Width
        {
            get { return width; }
            private set 
            {
	            if (value <= 0)
	                throw new ArgumentOutOfRangeException("Ширина цветовой плоскости должна быть больше 0");

				width = value;	            
            }
        }

        /// <summary>
        /// Имя канала изображения, который описывает экземпляр класса
        /// </summary>
        public string PlaneName
        {
            get { return planename; }
            internal set 
            { 
            	if (value == null) throw new NullReferenceException("Должно быть указано имя цветовой плосоксти");
            	planename = value; 
            }
        }
        
        /// <summary>
        /// Позволяет возвращать или задавать значение элемента цветовой плоскости c заданными координатами
        /// </summary>
        /// <param name="x">Координата элеметна цветовой плоскости по оси Х</param>
        /// <param name="y">Координата элеметна цветовой плоскости по оси У</param>
        /// <returns>
        /// Значение элемента цветовой плоскости, которое описывает 
        /// интенсивность сотсавляющей цвета пикселя в конкретной цветовой схеме
        /// </returns>
        public double this[int x, int y]
        {
            get 
            {
                return array[y, x];
            }
            set 
            { 
            	if (array[y, x] == value) return;
                
            	array[y, x] = value;
                
                if (Image != null) Image.NeedUpdate = true;
            }
        }

        private BoundaryValues GetBoundsForColor(ColorSchema schema, string planeName)
        {
            try
            {
                return boundaryValues[schema][planename];
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        public double MinValue
        {
            get
            {
                if (Image == null)
                    throw new InvalidOperationException("Невозможно определить цветовую схему. Плоскость не привязана к изображению");

                BoundaryValues boundaryValues = GetBoundsForColor(Image.Schema, PlaneName);

                if (boundaryValues == null)
                    throw new InvalidOperationException("Цветовая плоскость обладает недопустимым именем");
                else
                    return boundaryValues.Min;
            }
        }

        public double MaxValue 
        {
            get
            {
                if (Image == null)
                    throw new InvalidOperationException("Невозможно определить цветовую схему. Плоскость не привязана к изображению");

                BoundaryValues boundaryValues = GetBoundsForColor(Image.Schema, PlaneName);

                if (boundaryValues == null)
                    throw new InvalidOperationException("Цветовая плоскость обладает недопустимым именем");
                else
                    return boundaryValues.Max;
            }
        }

        /// <summary>
        /// Описывает изображение, в составе которого находится цветовая плоскость
        /// </summary>
        public ImageEx Image { get; set; }

        public override string ToString()
        {
            return "ColorPlane №" + counter;
        }

        /// <summary>
        /// Создает новый объект "цветовая плоскость" с заданной шириной и высотой
        /// </summary>
        /// <param name="height">Ширина создаваемой цветовой плоскости</param>
        /// <param name="width">Высота создаваемой цветовой плоскости</param>
        internal ColorPlane(int height, int width)
        {
            Height = height;
            Width = width;

            array = new double[Height, Width];

            Image = null;

            counter = GlobalCounter;
            GlobalCounter++;
        }

        internal ColorPlane(int height, int width, string planename) : this(height, width)
        {
            PlaneName = planename;            
        }

        #region Члены ICloneable

        /// <summary>
        /// Создает новый объект "цветовая плоскость", который является копией текущего экземпляра.
        /// </summary>
        /// <returns>Новый объект "цветовая плоскость, являющийся копией этого экземпляра.</returns>
        public object Clone()
        {
            ColorPlane rez = new ColorPlane(Height, Width);

            rez.array = (double[,])array.Clone();

            rez.PlaneName = PlaneName;
            rez.Image = Image;
            rez.RegionOfInterest = RegionOfInterest;

            return rez;
        }

        #endregion

        public void Eclipse(bool needUpdate)
        {
            if (Image == null) return;

            double UpperLimit = MaxValue;
            double LowerLimit = MinValue;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    if (array[y, x] < LowerLimit)
                    {
                        array[y, x] = LowerLimit;
                        continue;
                    }

                    if (array[y, x] > UpperLimit)
                    {
                        array[y, x] = UpperLimit;
                    }
                }
            }

            if (Image != null) Image.NeedUpdate = needUpdate;
        }

        public void Eclipse()
        {
            Eclipse(true);
        }

        static ColorPlane GetPlaneTemplate(ColorPlane plane1, ColorPlane plane2)
        {
            ColorPlane RezPlane = null;

            RezPlane = new ColorPlane(plane1.Height, plane1.Width);

            return RezPlane;
        }

        static void SetSettings(ColorPlane plane, ColorPlane RezPlane)
        {
            RezPlane.PlaneName = plane.PlaneName;
            RezPlane.Image = plane.Image;
            if (RezPlane.Image != null) RezPlane.Image.NeedUpdate = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plane1"></param>
        /// <param name="plane"></param>
        /// <returns></returns>
        public ColorPlane Add(ColorPlane plane)
        {
            ColorPlane RezPlane;

            if (RegionOfInterest == null)
            {
                RezPlane = GetPlaneTemplate(this, plane);
                if (Height == plane.Height && Width == plane.Width)
                {

                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = this[x, y] + plane[x, y];
                        }
                    }
                }
                else
                {
                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = this[x, y] + plane[x % plane.Width, y % plane.Height];
                        }
                    }
                }
            }
            else
            {
                RezPlane = (ColorPlane)Clone();

                foreach (Point point in RezPlane.RegionOfInterest)
                {
                    int x = point.X, y = point.Y;

                    RezPlane[x, y] = this[x, y] + plane[x % plane.Width, y % plane.Height]; 
                }
            }

            SetSettings(this, RezPlane);
            return RezPlane;
        }
        
        public ColorPlane Add(double k)
        {
            ColorPlane RezPlane = null;

            if (regionOfInterest == null)
            {
                RezPlane = new ColorPlane(Height, Width);

                for (int y = 0; y < RezPlane.Height; y++)
                {
                    for (int x = 0; x < RezPlane.Width; x++)
                    {
                        RezPlane[x, y] = this[x, y] + k;
                    }
                }
            }
            else
            {
                RezPlane = (ColorPlane)Clone();
                foreach (Point point in RezPlane.RegionOfInterest)
                {
                    int x = point.X, y = point.Y;

                    RezPlane[x, y] = this[x, y] + k;
                }
            }

            SetSettings(this, RezPlane);
            return RezPlane;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plane1"></param>
        /// <param name="plane"></param>
        /// <returns></returns>
        public ColorPlane Sub(ColorPlane plane)
        {
            ColorPlane RezPlane;

            if (RegionOfInterest == null)
            {
                RezPlane = GetPlaneTemplate(this, plane);
            
                if (Height == plane.Height && Width == plane.Width)
                {
                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = this[x, y] - plane[x, y];
                        }
                    }
                }
                else
                {
                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = this[x, y] - plane[x % plane.Width, y % plane.Height];
                        }
                    }
                }
            }
            else
            {
                RezPlane = (ColorPlane)Clone();

                foreach (Point point in RezPlane.RegionOfInterest)
                {
                    int x = point.X, y = point.Y;

                    RezPlane[x, y] = this[x, y] - plane[x % plane.Width, y % plane.Height];
                }
            }

            SetSettings(this, RezPlane); 
            return RezPlane;
        }

        public ColorPlane Sub(double k, bool kIsFirst)
        {
            ColorPlane RezPlane = null;

            if (RegionOfInterest == null)
            {
                RezPlane = new ColorPlane(Height, Width);

                if (!kIsFirst)
                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = this[x, y] - k;
                        }
                    }
                else
                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = k - this[x, y];
                        }
                    }
            }
            else
            {
                RezPlane = (ColorPlane)Clone();

                if (!kIsFirst)
                    foreach (Point point in RezPlane.RegionOfInterest)
                    {
                        int x = point.X, y = point.Y;

                        RezPlane[x, y] = this[x, y] - k;
                    }
                else
                    foreach (Point point in RezPlane.RegionOfInterest)
                    {
                        int x = point.X, y = point.Y;

                        RezPlane[x, y] = k - this[x, y];
                    }
            }

            SetSettings(this, RezPlane);
            return RezPlane;
        }

        public ColorPlane Minus()
        {
            return Mul(-1);

            //ColorPlane RezPlane;

            //if (RegionOfInterest == null)
            //{
            //    RezPlane = new ColorPlane(Height, Width);

            //    for (int yy = 0; yy < RezPlane.Height; yy++)
            //    {
            //        for (int xx = 0; xx < RezPlane.Width; xx++)
            //        {
            //            RezPlane[xx, yy] = -this[xx, yy];
            //        }
            //    }
            //}
            //else
            //{
            //    RezPlane = (ColorPlane)Clone();
            //    foreach (Point point in RezPlane.RegionOfInterest)
            //    {
            //        int xx = point.X, yy = point.Y;

            //        RezPlane[xx, yy] = -this[xx, yy];
            //    }
            //}
        
            //SetSettings(this, RezPlane);
            //return RezPlane;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="plane1"></param>
        /// <param name="plane"></param>
        /// <returns></returns>
        public ColorPlane Mul(ColorPlane plane)
        {
            ColorPlane RezPlane;

            if (RegionOfInterest == null)
            {
                RezPlane = GetPlaneTemplate(this, plane);

                if (Height == plane.Height && Width == plane.Width)
                {
                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = this[x, y] * plane[x, y];
                        }
                    }
                }
                else
                {
                    for (int y = 0; y < RezPlane.Height; y++)
                    {
                        for (int x = 0; x < RezPlane.Width; x++)
                        {
                            RezPlane[x, y] = this[x, y] * plane[x % plane.Width, y % plane.Height];
                        }
                    }
                }
            }
            else
            {
                RezPlane = (ColorPlane)Clone();

                foreach (Point point in RezPlane.RegionOfInterest)
                {
                    int x = point.X, y = point.Y;

                    RezPlane[x, y] = this[x, y] * plane[x % plane.Width, y % plane.Height];
                }
            }

            SetSettings(this, RezPlane); 
            return RezPlane;
        }

        public ColorPlane Mul(double k)
        {
            ColorPlane RezPlane = null;

            if (RegionOfInterest == null)
            {
                RezPlane = new ColorPlane(Height, Width);

                for (int y = 0; y < RezPlane.Height; y++)
                {
                    for (int x = 0; x < RezPlane.Width; x++)
                    {
                        RezPlane[x, y] = this[x, y] * k;
                    }
                }
            }
            else
            {
                RezPlane = (ColorPlane)Clone();
                foreach (Point point in RezPlane.RegionOfInterest)
                {
                    int x = point.X, y = point.Y;

                    RezPlane[x, y] = this[x, y] * k;
                }
            }

            SetSettings(this, RezPlane);
            return RezPlane;
        }

        public ColorPlane Equate(ColorPlane plane)
        {
            ColorPlane RezPlane = null;

            if (RegionOfInterest == null)
                RezPlane = (ColorPlane)plane.Clone();
            else
            {
                RezPlane = (ColorPlane)Clone();
                foreach (Point point in RezPlane.RegionOfInterest)
                {
                    int x = point.X, y = point.Y;

                    RezPlane[x, y] = plane[x, y];
                }
            }

            SetSettings(this, RezPlane);
            return RezPlane;
        }

        public void ApplyGradTransform(GradationTransform gradtransf)
        {
            if (Image == null)
                throw new InvalidOperationException("Операция не применима для отдельных цветовых плоскостей");

            if (Image.Schema != ColorSchema.RGB)
                throw new InvalidOperationException("Изображение должно быть в цветовой схеме RGB");

            if (regionOfInterest == null)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        array[y, x] = gradtransf[array[y, x]];
                    }
                }
            }
            else
            {
                foreach (Point point in regionOfInterest)
                {
                    int x = point.X, y = point.Y;

                    array[y, x] = gradtransf[array[y, x]];
                }
            }

            if (Image != null) Image.NeedUpdate = true;
        }

        double CalcFirstSum(double[,] arr, SpaceFilterMasck masck)
        {
            double sum = 0;
            int size = masck.Size;

            for (int y = 0, i = 0; y < size; y++, i++)
            {
                for (int x = 0, j = 0; x < size; x++, j++)
                {
                    sum += arr[y, x] * masck[i, j];
                }
            }

            return sum;
        }

        public void ApplyFilter(SpaceFilterMasck masck)
        {
            int edging = masck.Size / 2;
            ColorPlane extendedPlane = GetAdvancedPlane(edging);
            int size = masck.Size;
            int centr = masck.Size / 2;
            double sum = 0, memsum = 0;

            if (regionOfInterest == null)
            {
                switch (masck.Filter)
                {
                    case SpaceFilterType.Averaging:
                    {
                        double val = masck[0, 0];
                             
                        for (int y = 0, i = 0; y < size; y++, i++)
                        {
                            for (int x = 0, j = 0; x < size; x++, j++)
                            {
                                sum += extendedPlane.array[y, x] * val;
                            }
                        }
                        array[0, 0] = sum;

                        for (int y = 0; y < height - 1; y++)
                        {
                            memsum = sum;
                            for (int x = 1; x < width; x++)
                            {
                                for (int k = y, i = 0; k < y + size; k++, i++)
                                {
                                    sum -= extendedPlane.array[k, x - 1] * val;
                                    sum += extendedPlane.array[k, x + size - 1] * val;
                                }
                                array[y, x] = sum;
                            }

                            for (int x = 0, j = 0; x < size; x++, j++)
                            {
                                memsum -= extendedPlane.array[y, x] * val;
                                memsum += extendedPlane.array[y + size, x] * val;
                            }
                            sum = memsum;
                            array[y + 1, 0] = sum;
                        }

                        {
                            int y = height - 1;
                            memsum = sum;
                            for (int x = 1; x < width; x++)
                            {
                                for (int k = y, i = 0; k < y + size; k++, i++)
                                {
                                    sum -= extendedPlane.array[k, x - 1] * val;
                                    sum += extendedPlane.array[k, x + size - 1] * val;
                                }
                                array[y, x] = sum;
                            }
                        }
                    }
                    break;

                    case SpaceFilterType.PrevitHorizontal:
                    {
                        double minval = masck[size - 1, 0];
                        double maxval = masck[0, 0];

                        for (int y = 0; y < height; y++)
                        {
                            sum = 0;
                            for (int x = 0, j = 0; x < size; x++, j++)
                            {
                                sum += extendedPlane.array[y, x] * maxval;
                                sum += extendedPlane.array[y + size - 1, x] * minval;
                            }
                            array[y, 0] = sum;

                            for (int x = 1; x < width; x++)
                            {
                                int top = y;
                                int bottom = y + size - 1;
                                int left = x - 1;
                                int right = x + size - 1;

                                sum -= extendedPlane.array[top, left] * maxval;
                                sum -= extendedPlane.array[bottom, left] * minval;
                                sum += extendedPlane.array[top, right] * maxval;
                                sum += extendedPlane.array[bottom, right] * minval;

                                array[y, x] = sum;
                            }
                        }
                    }
                    break;

                    case SpaceFilterType.PrevitVertical:
                    {
                        double maxval = masck[0, size - 1];
                        double minval = masck[0, 0];

                        for (int x = 0; x < width; x++)
                        {
                            sum = 0;
                            for (int y = 0, i = 0; y < size; y++, i++)
                            {
                                sum += extendedPlane.array[y, x] * minval;
                                sum += extendedPlane.array[y, x + size - 1] * maxval;
                            }
                            array[0, x] = sum;

                            for (int y = 1; y < height; y++)
                            {
                                int top = y - 1;
                                int bottom = y + size - 1;
                                int left = x;
                                int right = x + size - 1;

                                sum -= extendedPlane.array[top, left] * minval;
                                sum -= extendedPlane.array[top, right] * maxval;
                                sum += extendedPlane.array[bottom, left] * minval;
                                sum += extendedPlane.array[bottom, right] * maxval;

                                array[y, x] = sum;
                            }
                        }
                    }
                    break;

                    case SpaceFilterType.SobelHorizontal:
                    {
                        double val = masck[0, 0];
                        double centrval = masck[0, centr];

                        for (int y = 0; y < height; y++)
                        {
                            sum = 0;
                            for (int x = 0, j = 0; x < size; x++, j++)
                            {
                                sum += extendedPlane.array[y, x] * masck[0, j];
                                sum += extendedPlane.array[y + size - 1, x] * masck[size - 1, j];
                            }
                            array[y, 0] = sum;

                            for (int x = 1; x < width; x++)
                            {
                                int top = y;
                                int bottom = y + size - 1;
                                int left = x - 1;
                                int right = x + size - 1;
                                int centrx = x + centr - 1;

                                sum -= extendedPlane.array[top, left] * val;
                                sum -= extendedPlane.array[bottom, left] * -val;
                                sum -= extendedPlane.array[top, centrx] * centrval;
                                sum -= extendedPlane.array[top, centrx + 1] * val;
                                sum -= extendedPlane.array[bottom, centrx] * -centrval;
                                sum -= extendedPlane.array[bottom, centrx + 1] * -val;

                                sum += extendedPlane.array[top, right] * val;
                                sum += extendedPlane.array[bottom, right] * -val;
                                sum += extendedPlane.array[top, centrx] * val;
                                sum += extendedPlane.array[top, centrx + 1] * centrval;
                                sum += extendedPlane.array[bottom, centrx] * -val;
                                sum += extendedPlane.array[bottom, centrx + 1] * -centrval;

                                array[y, x] = sum;
                            }
                        }
                    }
                    break;

                    case SpaceFilterType.SobelVertical:
                    {
                        double val = masck[0, size - 1];
                        double centrval = masck[centr, size - 1];
//                        double maxval = masck[0, size - 1];
//                        double minval = masck[0, 0];

                        for (int x = 0; x < width; x++)
                        {
                            sum = 0;
                            for (int y = 0, i = 0; y < size; y++, i++)
                            {
                                sum += extendedPlane.array[y, x] * masck[i, 0];
                                sum += extendedPlane.array[y, x + size - 1] * masck[i, size - 1];
                            }
                            array[0, x] = sum;

                            for (int y = 1; y < height; y++)
                            {
                                int top = y - 1;
                                int bottom = y + size - 1;
                                int left = x;
                                int right = x + size - 1;
                                int centry = y + centr - 1;                                

                                sum -= extendedPlane.array[top, left] * -val;
                                sum -= extendedPlane.array[top, right] * val;
                                sum -= extendedPlane.array[centry, left] * -centrval;
                                sum -= extendedPlane.array[centry, right] * centrval;
                                sum -= extendedPlane.array[centry + 1, left] * -val;
                                sum -= extendedPlane.array[centry + 1, right] * val;

                                sum += extendedPlane.array[bottom, left] * -val;
                                sum += extendedPlane.array[bottom, right] * val;
                                sum += extendedPlane.array[centry, left] * -val;
                                sum += extendedPlane.array[centry, right] * val;
                                sum += extendedPlane.array[centry + 1, left] * -centrval;
                                sum += extendedPlane.array[centry + 1, right] * centrval;

                                array[y, x] = sum;
                            }
                        }
                    }
                    break;

                    case SpaceFilterType.HighFrequency:
                    case SpaceFilterType.UpperHighFrequency:
                    case SpaceFilterType.Diffusing:
                    {
                        double val = masck[0, 0];
                        double centrval = masck[centr, centr];

                        for (int y = 0, i = 0; y < size; y++, i++)
                        {
                            for (int x = 0, j = 0; x < size; x++, j++)
                            {
                                sum += extendedPlane.array[y, x] * val;
                            }
                        }
                        sum -= extendedPlane.array[centr, centr] * val;
                        sum += extendedPlane.array[centr, centr] * centrval;
                        array[0, 0] = sum;

                        for (int y = 0; y < height - 1; y++)
                        {
                            memsum = sum;
                            for (int x = 1; x < width; x++)
                            {
                                for (int k = y, i = 0; k < y + size; k++, i++)
                                {
                                    sum -= extendedPlane.array[k, x - 1] * val;
                                    sum += extendedPlane.array[k, x + size - 1] * val;
                                }
                                sum -= extendedPlane.array[y + centr, x + centr - 1] * centrval;
                                sum -= extendedPlane.array[y + centr, x + centr] * val;

                                sum += extendedPlane.array[y + centr, x + centr - 1] * val;
                                sum += extendedPlane.array[y + centr, x + centr] * centrval;
                                array[y, x] = sum;
                            }

                            for (int x = 0, j = 0; x < size; x++, j++)
                            {
                                memsum -= extendedPlane.array[y, x] * val;
                                memsum += extendedPlane.array[y + size, x] * val;
                            }
                            memsum -= extendedPlane.array[y + centr, centr] * centrval;
                            memsum -= extendedPlane.array[y + centr + 1, centr] * val;

                            memsum += extendedPlane.array[y + centr, centr] * val;
                            memsum += extendedPlane.array[y + centr + 1, centr] * centrval;

                            sum = memsum;
                            array[y + 1, 0] = sum;
                        }

                        {
                            int y = height - 1;
                            memsum = sum;
                            for (int x = 1; x < width; x++)
                            {
                                for (int k = y, i = 0; k < y + size; k++, i++)
                                {
                                    sum -= extendedPlane.array[k, x - 1] * val;
                                    sum += extendedPlane.array[k, x + size - 1] * val;
                                }
                                array[y, x] = sum;
                            }
                        }
                    }
                    break;
                    
                    default:
                        for (int y = 0; y < height; y++)
                        {
                            for (int x = 0; x < width; x++)
                            {
                                sum = 0;

                                for (int k = y, i = 0; k < y + size; k++, i++)
                                {
                                    for (int m = x, j = 0; m < x + size; m++, j++)
                                    {
                                        sum += extendedPlane.array[k, m] * masck[i, j];
                                    }
                                }
                                array[y, x] = sum;
                            }
                        }
                        break;
                }
            }
            else
            {
                foreach (Point point in regionOfInterest)
                {
                    int x = point.X, y = point.Y;
                    sum = 0;

                    for (int k = y, i = 0; k < y + size; k++, i++)
                    {
                        for (int m = x, j = 0; m < x + size; m++, j++)
                        {
                            sum += extendedPlane.array[k, m] * masck[i, j];
                        }
                    }
                    array[y, x] = sum;
                }
            }

            if (Image != null) Image.NeedUpdate = true;

            extendedPlane = null;
            GC.Collect();
        }

        public void ApplyFilter(OrderFilterMasck masck, OrdderFilterType type)
        {
            int edging = masck.Size / 2;
            ColorPlane tempPlane = GetAdvancedPlane(edging);

            if (regionOfInterest == null)
            {
                for (int y = 0, yy = edging; y < height; y++, yy++)
                {
                    for (int x = 0, xx = edging; x < width; x++, xx++)
                    {
                        array[y, x] = masck.FillSelection(tempPlane, xx, yy, type);
                    }
                }
            }
            else
            {
                foreach (Point point in regionOfInterest)
                {                    
                    int x = point.X, y = point.Y;

                    array[y, x] = masck.FillSelection(tempPlane, x + edging, y + edging, type);
                }
            }

            if (Image != null) Image.NeedUpdate = true;

            tempPlane = null;
            GC.Collect();
        }

        public ColorPlane GetAdvancedPlane(int edging)
        {
        	if (edging < 0) throw new ArgumentOutOfRangeException("Ширина рамки не может быть меньше 0");
        	
        	if (edging == 0) return (ColorPlane)Clone();
        	
 	        ColorPlane rezult = new ColorPlane(height + 2 * edging, width + 2 * edging);

            for (int y = edging; y < height + edging; y++)            
            {
                for (int x = edging; x < width + edging; x++)            
                {
                    rezult.array[y, x] = array[y - edging, x - edging];
                }
            }

            for(int y = edging; y < height + edging; y++) for(int x = 0; x < edging; x++)	
	        {
		        rezult.array[y, x] = array[y - edging, 0];
                rezult.array[y, rezult.width - x - 1] = array[y - edging, width - 1];
	        }

            for(int x = 0; x < rezult.width; x++)
            {
                for(int y = 0; y < edging; y++)
                {
                    rezult.array[y, x] = rezult.array[edging, x];
                    rezult.array[rezult.height - y - 1, x] = rezult.array[rezult.height - edging - 1, x];
                }
            }

            return rezult;
        }
    }
}
