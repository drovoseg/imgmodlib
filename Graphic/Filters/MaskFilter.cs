﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 24.01.2013
 * Время: 10:43
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters
{
	/// <summary>
	/// Базовый класс фильтров изображений, в основе которого лежит использование двумерного 
	/// массива-маски с выделенным центральным элементом
	/// </summary>
	public abstract class MaskFilter<T> : Filter
	{
		T[,] masck = null;
		protected T[,] Masck
		{
			get { return masck; }
			set
			{
				if (value == null) throw new ArgumentNullException("Маска не задана");
				if (value.GetLength(0) != value.GetLength(1))
					throw new InvalidOperationException("Невозможно задать неквадратную маску");
				
				AssertMasckSize(value.GetLength(0));
				masck = value;
			}
		}		

        protected void AssertMasckSize(int size)
        {
            if (size < 3) throw new ArgumentOutOfRangeException("Маска должна иметь размер не менее 3 пикселей");
            if (size % 2 != 0) throw new ArgumentOutOfRangeException("Маска должна иметь четко выраженный центр");
        }
		
		public int Size 
		{
			get { return masck.GetLength(0); }
		}
		
        public T this[int i, int j]
        {
            get { return masck[i, j]; }
        }
	}
}
