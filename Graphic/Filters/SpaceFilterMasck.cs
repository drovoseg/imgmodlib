﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImgModLib.Graphic
{
    public class SpaceFilterMasck : ICloneable
    {
        double[,] masck = null;
        int centr;

        SpaceFilterType filter;

        public SpaceFilterType Filter { get { return filter; } }

        public int Size { get { return masck.GetLength(0); } }
        //public int Centr { get { return padding; } }

        public double this[int i, int j]
        {
            get { return masck[i, j]; }
            set { masck[i, j] = value; }
        }

        public SpaceFilterMasck(int size)
        {
            if (size < 3) throw new Exception("Маска должна иметь размер не менее 3 пикселей");
            if (size % 2 != 0) throw new Exception("Маска должна иметь четко выраженный центр");

            masck = new double[size, size];
            
            centr = size / 2;
            masck[centr, centr] = size * size;
            Normalize();

            filter = SpaceFilterType.UserFilter;
        }

        private SpaceFilterMasck()
        {
            masck = null;            
        }    

        public SpaceFilterMasck(double[,] array)
        {
            if (array.GetLength(0) != array.GetLength(1))
                throw new Exception("Невозможно создать неквадратную маску");

            if (array.GetLength(0) < 3) throw new Exception("Маска должна иметь размер не менее 3 пикселей");
            if (array.GetLength(0) % 2 == 0) 
                throw new Exception("Маска должна иметь четко выраженный центр");

            masck = array;
            centr = array.GetLength(0) / 2;
            Normalize();

            filter = SpaceFilterType.UserFilter;
        }
        
        public SpaceFilterMasck(SpaceFilterType filter, int size, double parametr)
        {
            if (size < 3) throw new Exception("Маска должна иметь размер не менее 3 пикселей");
            if (size % 2 == 0) throw new Exception("Маска должна иметь четко выраженный центр");
            if (filter == SpaceFilterType.UserFilter)
                throw new InvalidOperationException("Данный тип конструктора не поддерживает пользовательских фильтров");
            centr = size / 2;

            switch (filter)
            {
                case SpaceFilterType.Averaging:          masck = Averaging(size);                    break;
                case SpaceFilterType.Triangle:           masck = Trinagle(size);                     break;
                case SpaceFilterType.Diffusing:          masck = Diffusing(size, parametr);          break; 
                case SpaceFilterType.Gaussian:           masck = Gaussian(size, parametr);           break;
                case SpaceFilterType.HighFrequency:      masck = HighFrequency(size);                break;
                case SpaceFilterType.PrevitHorizontal:   masck = PrevitHorizontal(size);             break;
                case SpaceFilterType.PrevitVertical:     masck = PrevitVertical(size);               break;
                case SpaceFilterType.SobelHorizontal:    masck = SobelHorizontal(size);              break;
                case SpaceFilterType.SobelVertical:      masck = SobelVertical(size);                break;
                case SpaceFilterType.UpperHighFrequency: masck = UpperHighFrequency(size, parametr); break;
            }
            
            Normalize();
            this.filter = filter;
        }

        private double[,] Diffusing(int size, double alpha)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = -1;
                }
            }

            array[centr, centr] = size * size - 1 + alpha;

            return array;
        }

        private double[,] UpperHighFrequency(int size, double alpha)
        {
            if (alpha <= 0) throw new Exception("Недопустимое значение параметра");

            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = -1;
                }
            }

            array[centr, centr] = size * size - 1 + alpha;

            return array;            
        }

        private double[,] SobelVertical(int size)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                array[i, 0] = -1;
                array[i, size - 1] = 1;
            }

            array[centr, 0] = -(centr + 1);
            array[centr, size - 1] = centr + 1;

            return array;
        }

        private double[,] SobelHorizontal(int size)
        {
            double[,] array = new double[size, size];

            for (int j = 0; j < centr; j++)
            {
                array[0, j] = 1;
                array[size - 1, j] = -1;
            }

            array[0, centr] = centr + 1;
            array[size - 1, centr] = -(centr + 1);

            return array;
        }

        private double[,] PrevitVertical(int size)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                array[i, 0] = -1;
                array[i, size - 1] = 1;               
            }

            return array;
        }

        private double[,] PrevitHorizontal(int size)
        {
            double[,] array = new double[size, size];

            for (int j = 0; j < size; j++)
            {
                array[0, j] = 1;
                array[size - 1, j] = -1;
            }

            return array;
        }

        private double[,] Gaussian(int size, double sigma)
        {
            if (sigma < 1) throw new Exception("Недопустимое значение параметра");

            double[,] array = new double[size, size];

            for (int i = 0; i <= centr; i++)
            {
                for (int j = 0; j <= centr; j++)
                {
                    array[i, j] = Math.Exp(((i) * (i) + (j) * (j) + 1) 
                                / (2 * sigma * sigma));

                    array[size - i - 1, j] = array[i, j];
                    array[i, size - j - 1] = array[i, j];
                    array[size - i - 1, size - j - 1] = array[i, j];
                }
            }
            
            return array;
        }

        private double[,] HighFrequency(int size)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = -1;
                }
            }

            array[centr, centr] = size * size - 1;

            return array;
        }

        private double[,] Trinagle(int size)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i <= centr; i++)
            {
                for (int j = 0; j <= centr; j++)
                {
                    array[i, j] = i + j + 1;

                    array[size - i - 1, j] = array[i, j];
                    array[i, size - j - 1] = array[i, j];
                    array[size - i - 1, size - j - 1] = array[i, j];
                }
            }

            return array;
        }

        private double[,] Averaging(int size)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = 1;
                }
            }

            return array;
        }

        public void Normalize()
        {
            double sum = 0;
            int size = Size;

            foreach (double elem in masck) sum += elem;

            if (sum == 0) return;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    masck[i, j] /= sum;
                }
            }
        }

        #region Члены ICloneable

        public object Clone()
        {
            SpaceFilterMasck rezmasck = new SpaceFilterMasck();

            rezmasck.masck = (double[,])masck.Clone();
            rezmasck.centr = centr;
            rezmasck.filter = filter;

            return rezmasck;
        }

        #endregion

        public static SpaceFilterMasck operator *(SpaceFilterMasck masck, double val)
        {
            int size = masck.Size;
            SpaceFilterMasck rez = new SpaceFilterMasck(size);

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    rez[i, j] = masck[i, j] * val;
                }
            }
            rez.Normalize();
            rez.filter = SpaceFilterType.UserFilter;

            return rez;
        }

        public static SpaceFilterMasck operator *(double val, SpaceFilterMasck masck)
        {
            return masck * val;
        }

        public static SpaceFilterMasck operator *(SpaceFilterMasck m1, SpaceFilterMasck m2)
        {
            SpaceFilterMasck rez;
            int size;

            if (m1.Size != m2.Size)
                throw new Exception("Маски невозможно перемножить");

            size = m1.Size;
            rez = new SpaceFilterMasck(m1.Size);

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    rez[i, j] = m1[i, j] * m2[i, j];
                }
            }
            rez.Normalize();
            rez.filter = SpaceFilterType.UserFilter;

            return rez;
        }

        public static SpaceFilterMasck operator +(SpaceFilterMasck masck, double val)
        {
            int size = masck.Size;
            SpaceFilterMasck rez = new SpaceFilterMasck(size);

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    rez[i, j] = masck[i, j] + val;
                }
            }
            rez.Normalize();
            rez.filter = SpaceFilterType.UserFilter;

            return rez;
        }

        public static SpaceFilterMasck operator +(double val, SpaceFilterMasck masck)
        {
            return masck + val;
        }

        public static SpaceFilterMasck operator +(SpaceFilterMasck m1, SpaceFilterMasck m2)
        {
            SpaceFilterMasck rez;
            int size;

            if (m1.Size != m2.Size)
                throw new Exception("Маски невозможно сложить");

            size = m1.Size;
            rez = new SpaceFilterMasck(m1.Size);

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    rez[i, j] = m1[i, j] + m2[i, j];
                }
            }
            rez.Normalize();
            rez.filter = SpaceFilterType.UserFilter;

            return rez;
        }

        public static SpaceFilterMasck operator -(SpaceFilterMasck masck)
        {
            return -1 * masck;
        }

        public static SpaceFilterMasck operator -(SpaceFilterMasck masck, double val)
        {
            int size = masck.Size;
            SpaceFilterMasck rez = new SpaceFilterMasck(size);

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    rez[i, j] = masck[i, j] - val;
                }
            }
            rez.Normalize();
            rez.filter = SpaceFilterType.UserFilter;

            return rez;
        }

        public static SpaceFilterMasck operator -(double val, SpaceFilterMasck masck)
        {
            return masck - val;
        }

        public static SpaceFilterMasck operator -(SpaceFilterMasck m1, SpaceFilterMasck m2)
        {
            SpaceFilterMasck rez;
            int size;

            if (m1.Size != m2.Size)
                throw new Exception("Маски невозможно вычесть");

            size = m1.Size;
            rez = new SpaceFilterMasck(m1.Size);

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    rez[i, j] = m1[i, j] - m2[i, j];
                }
            }
            rez.Normalize();
            rez.filter = SpaceFilterType.UserFilter;

            return rez;
        }
    }
}
