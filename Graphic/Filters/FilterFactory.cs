﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 12.07.2013
 * Время: 16:28
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using ImgModLib.Graphic.Filters.Order;
using ImgModLib.Graphic.Filters.Order.Apertures;
using ImgModLib.Graphic.Filters.Spase;

namespace ImgModLib.Graphic.Filters
{
	/// <summary>
	/// Description of FilterFactory.
	/// </summary>
	public class FilterFactory
	{	
		public int Size { get; set; }		
		public ApertureType FilterApertureType { get; set; }
		public double Parameter { get; set; }
						
		public FilterFactory(int size, double parameter, ApertureType apertureType)
		{
			Size = size;
			Parameter = parameter;
			FilterApertureType = apertureType;
		}
		
		public FilterFactory() 
		{
			
		}
		
		public Filter CreateFilter(FilterType filterType)
		{
			switch (filterType)
			{
				case FilterType.Averaging: return new AveragingFilter(Size);
				case FilterType.Diffusing: return new DiffusingFilter(Size, Parameter);
				case FilterType.Gaussian: return new GaussianFilter(Size, Parameter);
				case FilterType.HighFrequency: return new HighFrequencyFilter(Size);
				case FilterType.Max: return new MaxFilter(AppertureFactory.CreateAperture(Size, FilterApertureType));
				case FilterType.Median: return new MedianFilter(AppertureFactory.CreateAperture(Size, FilterApertureType));
				case FilterType.Min: return new MinFilter(AppertureFactory.CreateAperture(Size, FilterApertureType));
				//case FilterType.PrevitFilter: return new PrevitFilter(Size);
				case FilterType.PrevitHorizontal: return new PrevitHorizontalFilter(Size);
				case FilterType.PrevitVertical: return new PrevitVerticalFilter(Size);
//				case FilterType.SobelFilter:
				case FilterType.SobelHorizontal: return new SobelHorizontalFilter(Size);
				case FilterType.SobelVertical: return new SobelVerticalFilter(Size);
				case FilterType.Triangle: return new TriangleFilter(Size);
				case FilterType.UpperHighFrequency: return new UpperHighFrequencyFilter(Size, Parameter);
				
				default: throw new Exception("Данный код не должен выполниться");
			}
		}
	}
}
