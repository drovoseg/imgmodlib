﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ImgModLib.Graphic.Filters.Order.Apertures;

namespace ImgModLib.Graphic
{

    public class OrderFilterMasck
    {
        bool[,] masck;
        double[] selection;
        ApertureType apertureType;
        int centr;

        public int Size { get { return masck.GetLength(0); } }
        public ApertureType ApertureType { get { return apertureType; } }
        public bool this[int i, int j] { get { return masck[i, j]; } }

        public OrderFilterMasck(bool[,] array)
        {
            int numelem = 0, size;
            if (array.GetLength(0) != array.GetLength(1))
                throw new Exception("Невозможно создать маску c рпазным количеством пискелей по ширине и высоте");

            size = array.GetLength(0);

            if (size < 3) throw new Exception("Маска должна иметь размер не менее 3 пикселей");
            if (size % 2 == 0)
                throw new Exception("Маска должна иметь четко выраженный центр");
            if (!array[size / 2, size / 2])
                throw new Exception("Значение центра маски должно быть = true");

            masck = array;

            foreach (bool val in array) if (val) numelem++;
            selection = new double[numelem];
            centr = numelem / 2;
            //Clear();
            
            apertureType = ApertureType.UserApperture;
        }

        public OrderFilterMasck(int size, ApertureType apperture)
        {
            if (size < 3) throw new Exception("Маска должна иметь размер не менее 3 пикселей");
            if (size % 2 == 0) throw new Exception("Маска должна иметь четко выраженный центр");
            if (apperture == ApertureType.UserApperture)
                throw new InvalidOperationException("Данный тип конструктора не поддерживает пользовательских аппертур");

            masck = new bool[size, size];

            switch (apperture)
            {
                case ApertureType.Square:
                    selection = new double[size * size];
                    for (int i = 0; i < size; i++)
                    {
                        for (int j = 0; j < size; j++)
                        {
                            masck[i, j] = true;
                        }
                    }
                    centr = size * size / 2;
                break;

                case ApertureType.Cross:
                    selection = new double[size * 2 - 1];
                    for (int i = 0; i < size; i++) masck[i, size / 2] = true;
                    for (int j = 0; j < size; j++)masck[size / 2, j] = true;
                    centr = (size * 2 - 1) / 2;
                break;

                case ApertureType.Circle:
                    int r = size / 2, numelem = 0;
		            for ( int i = -r; i <= r; i++ ) for ( int j = -r; j <= r; j++ )
                    {
			            if ( Math.Round(Math.Sqrt((double)(i*i+j*j))) <= r ) 
			            {
                            numelem++;		
				            masck[i + r, j + r] = true;
			            }
                    }
                    selection = new double[numelem];
                    centr = numelem / 2;
                break;
            }

            //Clear();
            this.apertureType = apperture;
        }

        ////private void Clear()
        ////{
        ////    for (int i = 0; i < selection.Length; i++) selection[i] = double.NegativeInfinity;            
        ////}

        public bool IsEnvironment(int i, int j)
        {
            return masck[i, j];
        }

        internal double FillSelection(ColorPlane plane, int x, int y, OrdderFilterType type)
        {
            int minx, maxx, miny, maxy;
            int size = Size;

            //Clear();

            minx = x - (size / 2);
            //if (minx < 0) minx = 0;

            maxx = x + (size / 2);
            //if (maxx >= plane.Width) maxx = plane.Width - 1;
                 
            miny = y - (size / 2);
            //if (miny < 0) miny = 0;

            maxy = y + (size / 2);
            //if (maxy >= plane.Height) maxy = plane.Height - 1;

            switch (apertureType)
            {
                case ApertureType.Square:
                    for (int yy = miny, i = 0; yy <= maxy; yy++, i++)
                    {
                        for (int xx = minx, j = 0; xx <= maxx; xx++, j++)
                        {
                            selection[i * size + j] = plane[xx, yy];
                        }
                    }
                    break;

                case ApertureType.Cross:
                    for (int yy = miny, i = 0; yy <= maxy; yy++, i++)
                    {
                        selection[i] = plane[x, yy];
                    }

                    for (int i = 0; i < size / 2; i++)
                    {
                        selection[2 * i + size] = plane[minx + i, y];
                        selection[2 * i + 1 + size] = plane[maxx - i, y];
                    }
                    break;

                case ApertureType.Circle:
                default:
                    int m = 0;
                    for (int yy = miny, i = 0; yy <= maxy; yy++, i++)
                    {
                        for (int xx = minx, j = 0; xx <= maxx; xx++, j++)
                        {
                            if (IsEnvironment(i, j))
                            {
                                selection[m] = plane[xx, yy];
                                m++;
                            }
                        }
                    }
                    break;
            }

            switch (type)
            {
                case OrdderFilterType.Minimum: return GetMin();
                case OrdderFilterType.Median: return GetMedian();
                case OrdderFilterType.Maximum: return GetMax();
                default: throw new Exception("Данное условие не должно никогда выполниться");
            }
        }

        void Sort(double[] arr, int n)
        {
            //double x;
            //int i, j, k;

            //for (i = 0; i <= n; i++)
            //{
            //    // цикл проходов, i - номер прохода
            //    k = i; x = arr[i];

            //    // поиск места элемента в готовой последовательности 
            //    for (j = i + 1; j < arr.Length; j++) if (arr[j] < x)
            //        {
            //            k = j;
            //            x = arr[j];
            //        }
            //    arr[k] = arr[i];
            //    arr[i] = x;
            //}

            int l, r;

            l = (arr.Length - 1) / 2;

            r = arr.Length - 1;

            while (l > 0)
            {                
                Sift(l, r, arr);
                l--;
            }

            while (r >= n)
            {
                double x = arr[0];
                arr[0] = arr[r];
                arr[r] = x;
                r--;

                Sift(l, r, arr);
            }
        }

        private void Sift(int l, int r, double[] arr)
        {
            int i, j;
            double x;

            i = l; j = 2 * i;
            x = arr[i];

            while (j <= r)
            {
                if (j < r && arr[j] < arr[j + 1]) j++;
                if (x >= arr[j]) goto label;
                arr[i] = arr[j];

                i = j;
                j = 2 * i;
            }

            label: arr[i] = x;
        }

        private double GetMax()
        {
            double max = selection[0];
            
            for (int i = 1; i < selection.Length; i++) 
                if (selection[i] > max) max = selection[i];

            return max;
        }        

        private double GetMedian()
        {
            Sort(selection, selection.Length / 2);

            if (selection.Length % 2 == 0)
            {
                return (selection[centr - 1] + selection[centr]) / 2;
            }
            else return selection[centr];
        }

        private double GetMin()
        {
            double min = selection[0];

            for (int i = 1; i < selection.Length; i++)
                if (selection[i] < min) min = selection[i];

            return min;
        }

    }
}
