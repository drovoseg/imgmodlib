﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 24.01.2013
 * Время: 10:28
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters
{
	/// <summary>
	/// Базовый калссв для всех фильтров изображений
	/// </summary>
	public abstract class Filter : ICloneable
	{
		/// <summary>
		/// Метод выполняет обработку переданой цветовой плоскости по алгоритму, заложенному в фильтре
		/// </summary>
		/// <param name="plane">Цветовая плоскость, подлежащая обработке</param>
		public abstract void Apply(ColorPlane plane);
		
		/// <summary>
		/// Метод выполняет обработку переданого изображения по алгоритму, заложенному в фильтре.
		/// </summary>
		/// <param name="image">Изображение, подлежащее обработке</param>
		public void Apply(ImageEx image)
		{
			ColorSchema schema = image.Schema;
			image.ToRGB();
			
			Apply(image.Planes["R"]);
			Apply(image.Planes["G"]);
			Apply(image.Planes["B"]);
			
			image.ToColorSchema(schema);
			
			image.NeedUpdate = true;
		}
		
		/// <summary>
		/// Создает полную копию фильтра, то есть выполняется глубокое копирование
		/// </summary>
		/// <returns>Полная копия фильтра со всеми его полями</returns>
		public abstract object Clone();
	}
}
