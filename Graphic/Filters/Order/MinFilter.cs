﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 29.01.2013
 * Время: 16:40
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using ImgModLib.Graphic.Filters.Order.Apertures;

namespace ImgModLib.Graphic.Filters.Order
{
	/// <summary>
	/// Description of MinFilter.
	/// </summary>
	public sealed class MinFilter : OrderFilter
	{
		public MinFilter(Aperture aperture) : base(aperture)
		{
			
		}
		
		protected override double GetValueFromSelection(double[] selection)
		{
			return GetMin(selection);
		}
		
		private double GetMin(double[] selection)
		{
            double min = selection[0];

            for (int i = 1; i < selection.Length; i++)
            {
                if (selection[i] < min) min = selection[i];
            }

            return min;
		}
		
		public override object Clone()
		{
			return new MinFilter((Aperture)FilterAperture.Clone());
		}
	}
}
