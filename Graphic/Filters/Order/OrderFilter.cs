﻿
/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 24.01.2013
 * Время: 10:31
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using System.Drawing;
using ImgModLib.Graphic.Filters.Order.Apertures;

namespace ImgModLib.Graphic.Filters.Order
{
	/// <summary>
	/// Description of OrderFilter.
	/// </summary>
	public abstract class OrderFilter : MaskFilter<bool>
	{
//        double[] selection;
        Aperture filterAperture;
        
        protected Aperture FilterAperture
        {
        	get { return filterAperture; }
        	set
        	{
        		if (value == null) throw new ArgumentNullException("Апертура не задана");
        		filterAperture = value;
        	}
        }
        
//        protected double[] Selection
//        {
//        	get 
//        }
        
		public OrderFilter(Aperture aperture)
		{
			Masck = aperture.GetAperture();
//			selection = aperture.GetSelection();
//			Centr = selection.Length / 2;
			
			FilterAperture = aperture;
		}
		
        public bool IsEnvironment(int i, int j)
        {
            return Masck[i, j];
        }
		
		public override void Apply(ColorPlane plane)
		{
            int edging = Size / 2;
            ColorPlane tempPlane = plane.GetAdvancedPlane(edging);
            double[] selection = filterAperture.GetSelection();

            if (plane.RegionOfInterest == null)
            {
                for (int y = 0, yy = edging; y < plane.Height; y++, yy++)
                {
                    for (int x = 0, xx = edging; x < plane.Width; x++, xx++)
                    {
                    	FillSelection(selection, tempPlane, xx, yy);
                    	plane[x, y] = GetValueFromSelection(selection);// FillSelection(tempPlane, xx, yy, type);
                    }
                }
            }
            else
            {
                foreach (Point point in plane.RegionOfInterest)
                {                    
                    int x = point.X, y = point.Y;

                	FillSelection(selection, tempPlane, x + edging, y + edging);
                	plane[x, y] = GetValueFromSelection(selection);// FillSelection(tempPlane, xx, yy, type);
                }
            }

            if (plane.Image != null) plane.Image.NeedUpdate = true;

            tempPlane = null;
            GC.Collect();
		}
		
		protected virtual void FillSelection(double[] selection, ColorPlane plane, int x, int y)
        {
            int minx, maxx, miny, maxy;
            int size = Size;

            minx = x - (size / 2);
            maxx = x + (size / 2);
            miny = y - (size / 2);
            maxy = y + (size / 2);
            
            int m = 0;
            for (int yy = miny, i = 0; yy <= maxy; yy++, i++)
            {
                for (int xx = minx, j = 0; xx <= maxx; xx++, j++)
                {
                    if (!IsEnvironment(i, j)) continue;
                    
                    selection[m] = plane[xx, yy];
                    m++;
                }
            }
            
//            switch (apperture)
//            {
//                case Apperture.Square:
//                    for (int yy = miny, i = 0; yy <= maxy; yy++, i++)
//                    {
//                        for (int xx = minx, j = 0; xx <= maxx; xx++, j++)
//                        {
//                            selection[i * size + j] = plane[xx, yy];
//                        }
//                    }
//                    break;
//
//                case Apperture.Cross:
//                    for (int yy = miny, i = 0; yy <= maxy; yy++, i++)
//                    {
//                        selection[i] = plane[x, yy];
//                    }
//
//                    for (int i = 0; i < size / 2; i++)
//                    {
//                        selection[2 * i + size] = plane[minx + i, y];
//                        selection[2 * i + 1 + size] = plane[maxx - i, y];
//                    }
//                    break;
//
//                case Apperture.Circle:
//                default:
//                    int m = 0;
//                    for (int yy = miny, i = 0; yy <= maxy; yy++, i++)
//                    {
//                        for (int xx = minx, j = 0; xx <= maxx; xx++, j++)
//                        {
//                            if (IsEnvironment(i, j))
//                            {
//                                selection[m] = plane[xx, yy];
//                                m++;
//                            }
//                        }
//                    }
//                    break;
//            }

//            switch (type)
//            {
//                case OrdderFilterType.Minimum: return GetMin();
//                case OrdderFilterType.Median: return GetMedian();
//                case OrdderFilterType.Maximum: return GetMax();
//                default: throw new Exception("Данное условие не должно никогда выполниться");
//            }
        }
		
		protected abstract double GetValueFromSelection(double[] selection);
	}
}
