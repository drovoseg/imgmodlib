﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 29.01.2013
 * Время: 19:05
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Order.Apertures
{
	/// <summary>
	/// Description of Aperture.
	/// </summary>
	public abstract class Aperture : ICloneable
	{
		int size;
		
		public int Size
		{
			get { return size; }
			
			private set
			{
				AssertApertureSize(value);
				
				size = value;
			}
		}
		
		protected Aperture(int size)
		{
			Size = size;
		}
		
        void AssertApertureSize(int size)
        {
            if (size < 3) throw new ArgumentOutOfRangeException("Aпертура должна иметь размер не менее 3 пикселей");
            if (size % 2 != 0) throw new ArgumentOutOfRangeException("Aпертура должна иметь четко выраженный центр");
        }

        public abstract bool[,] GetAperture();
        
        public abstract double[] GetSelection();
		
		public abstract object Clone();
	}
}
