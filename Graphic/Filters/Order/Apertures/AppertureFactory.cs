﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 21.07.2013
 * Время: 13:59
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Order.Apertures
{
	/// <summary>
	/// Description of AppertureFactory.
	/// </summary>
	public static class AppertureFactory
	{
		public static Aperture CreateAperture(int size, ApertureType apertureType)
		{
			switch (apertureType)
			{
				case ApertureType.Circle: return new CircleAperture(size);
				case ApertureType.Cross: return new CrossAperture(size);
				case ApertureType.Square: return new SquareAperture(size);
				
				default: throw new Exception("Данный код не должен выполниться");
			}
		}
	}
}
