﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ImgModLib.Graphic.Filters.Order.Apertures
{
	public enum ApertureType 
	{ 
		UserApperture, 
		Square, 
		Cross, 
		Circle 
	}
}
