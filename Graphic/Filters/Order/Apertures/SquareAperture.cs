﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 29.01.2013
 * Время: 19:07
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Order.Apertures
{
	/// <summary>
	/// Description of SquareAperture.
	/// </summary>
	public sealed class SquareAperture : Aperture
	{	
		public SquareAperture(int size) : base(size)
		{
			
		}
		
		public override double[] GetSelection()
		{
			return new double[Size * Size];
		}
		
		public override bool[,] GetAperture()
		{
			bool[,] aperture = new bool[Size, Size];
			
            for (int i = 0; i < Size; i++)
            {
                for (int j = 0; j < Size; j++)
                {
                    aperture[i, j] = true;
                }
            }
			
            return aperture;
		}
		
		public override object Clone()
		{
			return new SquareAperture(Size);
		}
	}
}
