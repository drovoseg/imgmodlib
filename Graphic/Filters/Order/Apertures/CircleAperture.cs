﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 29.01.2013
 * Время: 20:01
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Order.Apertures
{
	/// <summary>
	/// Description of CircleAperture.
	/// </summary>
	public sealed class CircleAperture : Aperture
	{
		bool[,] aperture = null;
		int numelem = 0;
		
		public CircleAperture(int size) : base(size)
		{
			bool[,] aperture = new bool[Size, Size];
			int r = Size / 2;
			
            for (int i = -r; i <= r; i++) for (int j = -r; j <= r; j++)
            {
	            if (Math.Round(Math.Sqrt((double)(i*i + j*j))) <= r) 
	            {
                    numelem++;		
		            aperture[i + r, j + r] = true;
	            }
            }
		}
		
		public override double[] GetSelection()
		{
			return new double[numelem];
		}
		
		public override bool[,] GetAperture()
		{
			return (bool[,])aperture.Clone();
		}
		
		public override object Clone()
		{
			return new CircleAperture(Size);
		}
	}
}
