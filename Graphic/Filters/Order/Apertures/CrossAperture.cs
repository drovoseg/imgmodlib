﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 29.01.2013
 * Время: 19:57
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Order.Apertures
{
	/// <summary>
	/// Description of CrossAperture.
	/// </summary>
	public sealed class CrossAperture : Aperture
	{	
		public CrossAperture(int size) : base(size)
		{
			
		}
		
		public override double[] GetSelection()
		{
			return new double[Size * 2 - 1];
		}
		
		public override bool[,] GetAperture()
		{
			bool[,] aperture = new bool[Size, Size];

            for (int i = 0; i < Size; i++) aperture[i, Size / 2] = true;
            for (int j = 0; j < Size; j++) aperture[Size / 2, j] = true;
			
            return aperture;			
		}
		
		public override object Clone()
		{
			return new CrossAperture(Size);
		}
	}
}
