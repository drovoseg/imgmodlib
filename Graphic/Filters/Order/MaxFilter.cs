﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 29.01.2013
 * Время: 16:40
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using ImgModLib.Graphic.Filters.Order.Apertures;

namespace ImgModLib.Graphic.Filters.Order
{
	/// <summary>
	/// Description of MaxFilter.
	/// </summary>
	public sealed class MaxFilter : OrderFilter
	{
		public MaxFilter(Aperture aperture) : base(aperture)
		{
		}
		
		protected override double GetValueFromSelection(double[] selection)
		{
			return GetMax(selection);
		}
		
		private double GetMax(double[] selection)
        {
            double max = selection[0];
            
            for (int i = 1; i < selection.Length; i++) 
            {
                if (selection[i] > max) max = selection[i];
            }

            return max;
        }        
		
		public override object Clone()
		{
			return new MaxFilter((Aperture)FilterAperture.Clone());
		}
	}
}
