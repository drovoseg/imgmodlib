﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 29.01.2013
 * Время: 16:40
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using ImgModLib.Graphic.Filters.Order.Apertures;

namespace ImgModLib.Graphic.Filters.Order
{
	/// <summary>
	/// Description of MedianFilter.
	/// </summary>
	public sealed class MedianFilter : OrderFilter
	{
		public MedianFilter(Aperture aperture) : base(aperture)
		{
		}
		
		protected override double GetValueFromSelection(double[] selection)
		{
			return GetMedian(selection);
		}
		
        private double GetMedian(double[] selection)
        {
        	uint selectionCentr = (uint)selection.Length / 2;
            Sort(selection, selectionCentr);

            if (selection.Length % 2 == 0)
            {
                return (selection[selectionCentr - 1] + selection[selectionCentr]) / 2;
            }
            else return selection[selectionCentr];
        }
		
        void Sort(double[] arr, uint n)
        {
            uint l, r;

            l = ((uint)arr.Length - 1) / 2;

            r = (uint)arr.Length - 1;

            while (l > 0)
            {                
                Sift(l, r, arr);
                l--;
            }

            while (r >= n)
            {
                double x = arr[0];
                arr[0] = arr[r];
                arr[r] = x;
                r--;

                Sift(l, r, arr);
            }
        }

        private void Sift(uint l, uint r, double[] arr)
        {
            uint i, j;
            double x;

            i = l; j = 2 * i;
            x = arr[i];

            while (j <= r)
            {
                if (j < r && arr[j] < arr[j + 1]) j++;
                if (x >= arr[j]) goto label;
                arr[i] = arr[j];

                i = j;
                j = 2 * i;
            }

            label: arr[i] = x;
        }
        
		public override object Clone()
		{
			return new MedianFilter((Aperture)FilterAperture.Clone());
		}
	}
}
