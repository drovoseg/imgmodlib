﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:38
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of PrevitHorizontalFilter.
	/// </summary>
	public sealed class PrevitHorizontalFilter : SpaceFilter
	{
		private PrevitHorizontalFilter()
		{
		}
		
		public PrevitHorizontalFilter(int size) : base(size)
		{
			Masck = PrevitHorizontal(size);
			Normalize();
		}
		
        private double[,] PrevitHorizontal(int size)
        {
            double[,] array = new double[size, size];

            for (int j = 0; j < size; j++)
            {
                array[0, j] = 1;
                array[size - 1, j] = -1;
            }

            return array;
        }
        
		protected override void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
            int size = Size;
            int height = plane.Height;
            int width = plane.Width;
            
			double sum = 0;
            double minval = Masck[size - 1, 0];
            double maxval = Masck[0, 0];

            for (int y = 0; y < height; y++)
            {
                sum = 0;
                for (int x = 0, j = 0; x < size; x++, j++)
                {
                	sum += extendedPlane[x, y] * maxval;
                	sum += extendedPlane[x, y + size - 1] * minval;
                }
                plane[0, y] = sum;

                for (int x = 1; x < width; x++)
                {
                    int top = y;
                    int bottom = y + size - 1;
                    int left = x - 1;
                    int right = x + size - 1;

                    sum -= extendedPlane[left, top] * maxval;
                    sum -= extendedPlane[left, bottom] * minval;
                    sum += extendedPlane[right, top] * maxval;
                    sum += extendedPlane[right, bottom] * minval;

                    plane[x, y] = sum;
                }
            }
		}
		
		public override object Clone()
		{
			PrevitHorizontalFilter clone = new PrevitHorizontalFilter();
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
