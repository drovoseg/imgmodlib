﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:43
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of SobelHorizontalFilter.
	/// </summary>
	public sealed class SobelHorizontalFilter : SpaceFilter
	{
		private SobelHorizontalFilter()
		{
		}
		
		public SobelHorizontalFilter(int size) : base(size)
		{
			Masck = SobelHorizontal(size);
			Normalize();
		}

        private double[,] SobelHorizontal(int size)
        {
        	int centr = size / 2;
            double[,] array = new double[size, size];

            for (int j = 0; j < centr; j++)
            {
                array[0, j] = 1;
                array[size - 1, j] = -1;
            }

            array[0, centr] = centr + 1;
            array[size - 1, centr] = -(centr + 1);

            return array;
        }
        
		protected override void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
			double[,] masck = Masck;
			
			int size = Size;
			int centr = size / 2;
			int height = plane.Height;
			int width = plane.Width;
			
			double sum = 0;
            double val = masck[0, 0];
            double centrval = masck[0, centr];

            for (int y = 0; y < height; y++)
            {
                sum = 0;
                for (int x = 0, j = 0; x < size; x++, j++)
                {
                	sum += extendedPlane[x, y] * masck[0, j];
                	sum += extendedPlane[x, y + size - 1] * masck[size - 1, j];
                }
                plane[0, y] = sum;

                for (int x = 1; x < width; x++)
                {
                    int top = y;
                    int bottom = y + size - 1;
                    int left = x - 1;
                    int right = x + size - 1;
                    int centrx = x + centr - 1;

                    sum -= extendedPlane[left, top] * val;
                    sum -= extendedPlane[left, bottom] * -val;
                    sum -= extendedPlane[centrx, top] * centrval;
                    sum -= extendedPlane[centrx + 1, top] * val;
                    sum -= extendedPlane[centrx, bottom] * -centrval;
                    sum -= extendedPlane[centrx + 1, bottom] * -val;

                    sum += extendedPlane[right, top] * val;
                    sum += extendedPlane[right, bottom] * -val;
                    sum += extendedPlane[centrx, top] * val;
                    sum += extendedPlane[centrx + 1, top] * centrval;
                    sum += extendedPlane[centrx, bottom] * -val;
                    sum += extendedPlane[centrx + 1, bottom] * -centrval;

                    plane[x, y] = sum;
                }
            }
		}
        
		public override object Clone()
		{
			SobelHorizontalFilter clone = new SobelHorizontalFilter();
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
