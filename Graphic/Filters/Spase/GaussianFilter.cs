﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:36
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of GaussianFilter.
	/// </summary>
	public sealed class GaussianFilter : SpaceFilter
	{
		public GaussianFilter(int size) : base(size)
		{
		}
		
		public GaussianFilter(int size, double sigma) : this(size)
		{
			Masck = Gaussian(size, sigma);
			Normalize();
		}
		
        private double[,] Gaussian(int size, double sigma)
        {
            if (sigma < 1) throw new Exception("Недопустимое значение параметра");

            int centr = size / 2;
            double[,] array = new double[size, size];

            for (int i = 0; i <= centr; i++)
            {
                for (int j = 0; j <= centr; j++)
                {
                    array[i, j] = Math.Exp(((i) * (i) + (j) * (j) + 1) 
                                / (2 * sigma * sigma));

                    array[size - i - 1, j] = array[i, j];
                    array[i, size - j - 1] = array[i, j];
                    array[size - i - 1, size - j - 1] = array[i, j];
                }
            }
            
            return array;
        }
			
		public override object Clone()
		{
			GaussianFilter clone = new GaussianFilter(Size);
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
