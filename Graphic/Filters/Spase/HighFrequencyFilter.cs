﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:37
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of HighFrequencyFilter.
	/// </summary>
	public sealed class HighFrequencyFilter : SpaceFilter
	{
		private HighFrequencyFilter()
		{
		}
		
		public HighFrequencyFilter(int size) : base(size)
		{
			Masck = HighFrequency(size);
			Normalize();
		}
		
        private double[,] HighFrequency(int size)
        {
        	int centr = size / 2;
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = -1;
                }
            }

            array[centr, centr] = size * size - 1;

            return array;
        }
        
		protected override void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
			DiffusingFilter.ConvolveWithMasck(this, plane, extendedPlane);
		}
		
		public override object Clone()
		{
			HighFrequencyFilter clone = new HighFrequencyFilter();
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
