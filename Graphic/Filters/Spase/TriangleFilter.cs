﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:25
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of TriangleFilter.
	/// </summary>
	public sealed class TriangleFilter : SpaceFilter
	{
		private TriangleFilter()
		{
		}
		
		public TriangleFilter(int size) : base(size)
		{
			Masck = Triangle(size);
			Normalize();
		}
		
        private double[,] Triangle(int size)
        {
        	int centr = size / 2;
            double[,] array = new double[size, size];

            for (int i = 0; i <= centr; i++)
            {
                for (int j = 0; j <= centr; j++)
                {
                    array[i, j] = i + j + 1;

                    array[size - i - 1, j] = array[i, j];
                    array[i, size - j - 1] = array[i, j];
                    array[size - i - 1, size - j - 1] = array[i, j];
                }
            }

            return array;
        }
        
		public override object Clone()
		{
			TriangleFilter clone = new TriangleFilter();
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
