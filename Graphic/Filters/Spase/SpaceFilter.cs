﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 24.01.2013
 * Время: 10:32
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of SpaceFilter.
	/// </summary>
	public abstract class SpaceFilter : MaskFilter<double>
	{
//		int centr = 0;		
//		protected int Centr 
//		{
//			get { return centr; }
//			set
//			{
//				if (value < 0) throw new ArgumentOutOfRangeException("Координата центра должна быть положительной");
//				centr = value;
//			}
//		}
		
		protected SpaceFilter()
		{
		}
		
        protected SpaceFilter(int size)
        {
        	AssertMasckSize(size);
        	
//            Masck = new double[size, size];
        }
		
        
        public void Normalize()
        {
        	double[,] masck = Masck;
            double sum = 0;
            int size = Size;

            foreach (double elem in masck) sum += elem;

            if (sum == 0) return;

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    masck[i, j] /= sum;
                }
            }
        }
		
		public override void Apply(ColorPlane plane)
		{
            int edging = Size / 2;
            ColorPlane extendedPlane = plane.GetAdvancedPlane(edging);
            
            if (plane.RegionOfInterest == null)
            {	
            	ConvolveWithMasck(plane, extendedPlane);
            }
            else
            {
            	ConvolveWithMasck(plane, extendedPlane, plane.RegionOfInterest);
            }
		}
		
		protected virtual void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane, List<Point> regionOfInterest)
		{
            double[,] masck = Masck;
            int size = Size;

            foreach (Point point in regionOfInterest)
	        {
	            int x = point.X, y = point.Y;
	            double sum = 0;
	
	            for (int yy = y, i = 0; yy < y + size; yy++, i++)
	            {
	                for (int xx = x, j = 0; xx < x + size; xx++, j++)
	                {
	                	sum += extendedPlane[xx, yy] * masck[i, j];
	                }
	            }
	            
	            plane[x, y] = sum;
	        }
		}
		
		protected virtual void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
            double[,] masck = Masck;
            int size = Size;
            int height = plane.Height;
            int width = plane.Width;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    double sum = 0;

                    for (int k = y, i = 0; k < y + size; k++, i++)
                    {
                        for (int m = x, j = 0; m < x + size; m++, j++)
                        {
                        	sum += extendedPlane[m, k] * masck[i, j];
                        }
                    }
                    
                    plane[x, y] = sum;
                }
            }
		}
	}
}
