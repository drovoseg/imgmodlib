﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:13
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Drawing;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of AveragingFilter.
	/// </summary>
	public sealed class AveragingFilter : SpaceFilter
	{
		private AveragingFilter()
		{
		}
		
		public AveragingFilter(int size) : base(size)
		{
			Masck = Averaging(size);
			Normalize();
		}
		
		private double[,] Averaging(int size)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = 1;
                }
            }

            return array;
        }
		
		override protected void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane, List<Point> regionOfInterest)
		{
			int size = Size;			

			double sum = 0;
            double val = Masck[0, 0];
            
            foreach (IGrouping<int, Point> layerGroup in regionOfInterest.GroupBy(point => point.Y))
            {
            	List<Point> points = layerGroup.OrderBy(point => point.X).ToList();
            	int curY = layerGroup.Key;            	
            	
            	while(points.Count != 0)
            	{
            		int i = 0;
            		int startX = 0, endX = 0;
            		
            		while (points[i + 1].X - points[i].X == 1 && i < points.Count - 1)
            		{
            			i++;
            		}
            		
            		startX = points[0].X;
            		endX = points[i].X;
            		
		            for (int y = curY; y < curY + size; y++)
		            {
		                for (int x = startX; x < startX + size; x++)
		                {
		                	sum += extendedPlane[x, y] * val;
		                }
		            }

            		plane[startX, curY] = sum;
            		
	                for (int x = startX + 1; x <= endX; x++)
	                {
	                    for (int yy = curY; yy < curY + size; yy++)
	                    {
	                    	sum -= extendedPlane[x - 1, yy] * val;
	                    	sum += extendedPlane[x + size - 1, yy] * val;
	                    }
	                    plane[x, curY] = sum;
	                }
            		
	                points.RemoveRange(0, i + 1);
            	}
            }
		}
		
		override protected void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
			int size = Size;
			int height = plane.Height;
			int width = plane.Width;
			
			double sum = 0, memsum = 0;
            double val = Masck[0, 0];
            
            for (int y = 0, i = 0; y < size; y++, i++)
            {
                for (int x = 0, j = 0; x < size; x++, j++)
                {
                	sum += extendedPlane[x, y] * val;
                }
            }

            plane[0, 0] = sum;

            for (int y = 0; y < height - 1; y++)
            {
                memsum = sum;
                for (int x = 1; x < width; x++)
                {
                    for (int yy = y, i = 0; yy < y + size; yy++, i++)
                    {
                    	sum -= extendedPlane[x - 1, yy] * val;
                    	sum += extendedPlane[x + size - 1, yy] * val;
                    }
                    plane[x, y] = sum;
                }
                
//                if (y == height - 1) break;

                for (int x = 0, j = 0; x < size; x++, j++)
                {
                	memsum -= extendedPlane[x, y] * val;
                	memsum += extendedPlane[x, y + size] * val;
                }
                sum = memsum;
                plane[0, y + 1] = sum;
            }

            {
                int y = height - 1;
                memsum = sum;
                for (int x = 1; x < width; x++)
                {
                    for (int yy = y, i = 0; yy < y + size; yy++, i++)
                    {
                    	sum -= extendedPlane[x - 1, yy] * val;
                    	sum += extendedPlane[x + size - 1, yy] * val;
                    }
                    plane[x, y] = sum;
                }
            }			
		}
		
//		public override void Apply(ColorPlane plane)
//		{
//            int edging = Size / 2; 
//            ColorPlane extendedPlane = plane.GetAdvancedPlane(edging);
//			
//            if (plane.RegionOfInterest != null)
//            {
//            	ConvolveWithMasck(plane, extendedPlane, plane.RegionOfInterest);
//            }
//            else
//            {
//            	ConvolveWithMasck(plane, extendedPlane);
//            }
//		}
		
		public override object Clone()
		{
			AveragingFilter clone = new AveragingFilter();
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
