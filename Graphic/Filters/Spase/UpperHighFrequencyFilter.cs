﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:43
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of UpperHighFrequencyFilter.
	/// </summary>
	public sealed class UpperHighFrequencyFilter : SpaceFilter
	{
		private UpperHighFrequencyFilter(int size) : base(size)
		{
		}
		
		public UpperHighFrequencyFilter(int size, double alpha) : this(size)
		{
			Masck = UpperHighFrequency(size, alpha);
			Normalize();
		}
		
        private double[,] UpperHighFrequency(int size, double alpha)
        {
            if (alpha <= 0) throw new Exception("Недопустимое значение параметра");

            int centr = size / 2;
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = -1;
                }
            }

            array[centr, centr] = size * size - 1 + alpha;

            return array;            
        }

        protected override void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
			DiffusingFilter.ConvolveWithMasck(this, plane, extendedPlane);
		}
		
		public override object Clone()
		{
			UpperHighFrequencyFilter clone = new UpperHighFrequencyFilter(Size);
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
