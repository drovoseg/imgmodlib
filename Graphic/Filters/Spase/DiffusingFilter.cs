﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 30.01.2013
 * Время: 18:26
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;
using System.Collections.Generic;
using System.Drawing;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of DiffusingFilter.
	/// </summary>
	public sealed class DiffusingFilter : SpaceFilter
	{
		public DiffusingFilter(int size) : base(size)
		{
		}
		
		public DiffusingFilter(int size, double alpha) : this(size)
		{
			Masck = Diffusing(size, alpha);
			Normalize();
		}
		
        private double[,] Diffusing(int size, double alpha)
        {
        	int centr = size / 2;
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    array[i, j] = -1;
                }
            }

            array[centr, centr] = size * size - 1 + alpha;

            return array;
        }
		
		protected override void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
			ConvolveWithMasck(this, plane, extendedPlane);
		}
		
		static internal void ConvolveWithMasck(SpaceFilter filter, ColorPlane plane, ColorPlane extendedPlane)
		{	
			int size = filter.Size;
			int centr = size / 2;
			int height = plane.Height;
			int width = plane.Width;
			
			double sum = 0, memsum = 0;
			double val = filter[0, 0];
            double centrval = filter[centr, centr];

            for (int y = 0, i = 0; y < size; y++, i++)
            {
                for (int x = 0, j = 0; x < size; x++, j++)
                {
                	sum += extendedPlane[x, y] * val;
                }
            }
            sum -= extendedPlane[centr, centr] * val;
            sum += extendedPlane[centr, centr] * centrval;
            plane[0, 0] = sum;

            for (int y = 0; y < height - 1; y++)
            {
                memsum = sum;
                for (int x = 1; x < width; x++)
                {
                    for (int k = y, i = 0; k < y + size; k++, i++)
                    {
                        sum -= extendedPlane[x - 1, k] * val;
                        sum += extendedPlane[x + size - 1, k] * val;
                    }
                    sum -= extendedPlane[x + centr - 1, y + centr] * centrval;
                    sum -= extendedPlane[x + centr, y + centr] * val;

                    sum += extendedPlane[x + centr - 1, y + centr] * val;
                    sum += extendedPlane[x + centr, y + centr] * centrval;
                    plane[x, y] = sum;
                }

                for (int x = 0, j = 0; x < size; x++, j++)
                {
                	memsum -= extendedPlane[x, y] * val;
                	memsum += extendedPlane[x, y + size] * val;
                }
                memsum -= extendedPlane[centr, y + centr] * centrval;
                memsum -= extendedPlane[centr, y + centr + 1] * val;

                memsum += extendedPlane[centr, y + centr] * val;
                memsum += extendedPlane[centr, y + centr + 1] * centrval;

                sum = memsum;
                plane[0, y + 1] = sum;            
            }

            {
                int y = height - 1;
                memsum = sum;
                for (int x = 1; x < width; x++)
                {
                    for (int k = y, i = 0; k < y + size; k++, i++)
                    {
                        sum -= extendedPlane[x - 1, k] * val;
                        sum += extendedPlane[x + size - 1, k] * val;
                    }
                    plane[x, y] = sum;
                }
            }			
		}
		
		public override object Clone()
		{
			DiffusingFilter clone = new DiffusingFilter(Size);
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
