﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 09.02.2013
 * Время: 7:39
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of PrevitVerticalFilter.
	/// </summary>
	public sealed class PrevitVerticalFilter : SpaceFilter
	{
		private PrevitVerticalFilter()
		{
		}
		
		public PrevitVerticalFilter(int size) : base(size)
		{
			Masck = PrevitVertical(size);
			Normalize();
		}
		
        private double[,] PrevitVertical(int size)
        {
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                array[i, 0] = -1;
                array[i, size - 1] = 1;               
            }

            return array;
        }
		
		protected override void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
            int size = Size;
            int height = plane.Height;
            int width = plane.Width;
            
            double sum = 0;
            double maxval = Masck[0, Size - 1];
            double minval = Masck[0, 0];

            for (int x = 0; x < width; x++)
            {
                sum = 0;
                for (int y = 0, i = 0; y < size; y++, i++)
                {
                	sum += extendedPlane[x, y] * minval;
                	sum += extendedPlane[x + size - 1, y] * maxval;
                }
                plane[x, 0] = sum;

                for (int y = 1; y < height; y++)
                {
                    int top = y - 1;
                    int bottom = y + size - 1;
                    int left = x;
                    int right = x + size - 1;

                    sum -= extendedPlane[left, top] * minval;
                    sum -= extendedPlane[right, top] * maxval;
                    sum += extendedPlane[left, bottom] * minval;
                    sum += extendedPlane[right, bottom] * maxval;

                    plane[x, y] = sum;
                }
            }
		}
        
		public override object Clone()
		{
			PrevitVerticalFilter clone = new PrevitVerticalFilter();

			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
