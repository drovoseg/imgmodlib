﻿/*
 * Сделано в SharpDevelop.
 * Пользователь: Евгений
 * Дата: 30.01.2013
 * Время: 18:19
 * 
 * Для изменения этого шаблона используйте Сервис | Настройка | Кодирование | Правка стандартных заголовков.
 */
using System;

namespace ImgModLib.Graphic.Filters.Spase
{
	/// <summary>
	/// Description of SobelVerticalFilter.
	/// </summary>
	public sealed class SobelVerticalFilter : SpaceFilter
	{
		private SobelVerticalFilter()
		{
		}
		
		public SobelVerticalFilter(int size) : base(size)
		{
			Masck = SobelVertical(size);
			Normalize();
		}
		
        private double[,] SobelVertical(int size)
        {
        	int centr = size / 2;
            double[,] array = new double[size, size];

            for (int i = 0; i < size; i++)
            {
                array[i, 0] = -1;
                array[i, size - 1] = 1;
            }

            array[centr, 0] = -(centr + 1);
            array[centr, size - 1] = centr + 1;

            return array;
        }
		
		protected override void ConvolveWithMasck(ColorPlane plane, ColorPlane extendedPlane)
		{
			double[,] masck = Masck;
			
			int size = Size;
			int centr = size / 2;
			int height = plane.Height;
			int width = plane.Width;

			double sum = 0;
			double val = masck[0, size - 1];
            double centrval = masck[centr, size - 1];

            for (int x = 0; x < width; x++)
            {
                sum = 0;
                for (int y = 0, i = 0; y < size; y++, i++)
                {
                	sum += extendedPlane[x, y] * masck[i, 0];
                	sum += extendedPlane[x + size - 1, y] * masck[i, size - 1];
                }
                plane[x, 0] = sum;

                for (int y = 1; y < height; y++)
                {
                    int top = y - 1;
                    int bottom = y + size - 1;
                    int left = x;
                    int right = x + size - 1;
                    int centry = y + centr - 1;                                

                    sum -= extendedPlane[left, top] * -val;
                    sum -= extendedPlane[right, top] * val;
                    sum -= extendedPlane[left, centry] * -centrval;
                    sum -= extendedPlane[right, centry] * centrval;
                    sum -= extendedPlane[left, centry + 1] * -val;
                    sum -= extendedPlane[right, centry + 1] * val;

                    sum += extendedPlane[left, bottom] * -val;
                    sum += extendedPlane[right, bottom] * val;
                    sum += extendedPlane[left, centry] * -val;
                    sum += extendedPlane[right, centry] * val;
                    sum += extendedPlane[left, centry + 1] * -centrval;
                    sum += extendedPlane[right, centry + 1] * centrval;

                    plane[x, y] = sum;
                }
            }
		}
        
		public override object Clone()
		{
			SobelVerticalFilter clone = new SobelVerticalFilter();
			
			clone.Masck = (double[,])this.Masck.Clone();
			
			return clone;
		}
	}
}
 